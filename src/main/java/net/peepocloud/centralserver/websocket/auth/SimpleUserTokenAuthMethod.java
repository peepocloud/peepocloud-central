package net.peepocloud.centralserver.websocket.auth;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.login.APICloudData;
import net.peepocloud.centralserver.login.Account;
import net.peepocloud.centralserver.websocket.WebSocket;

public class SimpleUserTokenAuthMethod implements WebSocketServerAuthMethod {
    @Override
    public boolean checkAuth(WebSocket webSocket, SimpleJsonObject jsonObject) {
        String username = jsonObject.getString("user");
        String apiToken = jsonObject.getString("apiToken");
        if (username == null || apiToken == null)
            return false;

        Account account = CentralServer.getInstance().getLoginManager().checkAuthAPIAndGetAccount(username, apiToken);
        if (account == null)
            return false;
        String uniqueId = null;
        for (APICloudData apiData : account.getApiData()) {
            if (apiData.getApiToken().equals(apiToken)) {
                uniqueId = apiData.getUniqueId();
                break;
            }
        }
        if (uniqueId == null)
            return false;
        webSocket.setUsername(username);
        webSocket.setUniqueId(uniqueId);
        return true;
    }
}

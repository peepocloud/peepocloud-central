package net.peepocloud.centralserver.discord.userinput;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import lombok.*;
import net.dv8tion.jda.core.entities.Message;

import java.util.function.Consumer;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UserTextInput {
    private long memberId;
    private long channelId;
    private Consumer<Message> consumer;
    private long expireMillis;
    private Runnable expired;
}

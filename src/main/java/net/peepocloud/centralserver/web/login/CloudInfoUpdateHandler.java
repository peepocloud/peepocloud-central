package net.peepocloud.centralserver.web.login;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfo;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.utility.Hashing;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

public class CloudInfoUpdateHandler extends HttpHandler { //TODO extra ban (included in support ban), so if you get support banned, your cloud also gets banned but only from THIS handler
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String hashedIpAddress = Hashing.hashString(httpExchange.getRemoteAddress().getAddress().getHostAddress());
        PeepoCloudInfo cloudInfo;
        try (InputStream inputStream = httpExchange.getRequestBody();
             Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
            SimpleJsonObject request = new SimpleJsonObject(reader);
            String username = request.getString("username");
            String apiToken = request.getString("apiToken");
            cloudInfo = request.getObject("cloudInfo", PeepoCloudInfo.class);
            if (cloudInfo == null ||
                    cloudInfo.getUniqueId() == null ||
                    username == null ||
                    apiToken == null ||
                    !CentralServer.getInstance().getLoginManager().checkAuthAPI(username, cloudInfo.getUniqueId(), apiToken)) {
                send(
                        httpExchange,
                        400,
                        new SimpleJsonObject()
                                .append("success", false)
                                .append("reason", Collections.singletonList("Invalid json request"))
                                .toBytes()
                );
                return;
            }
        } catch (Exception e) {
            send(
                    httpExchange,
                    400,
                    new SimpleJsonObject()
                            .append("success", false)
                            .append("reason", Collections.singletonList("Invalid json request"))
                            .toBytes()
            );
            return;
        }
        boolean success = CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().updateInfo(hashedIpAddress, cloudInfo);
        send(
                httpExchange,
                success ? 200 : 400,
                success ?
                        new SimpleJsonObject()
                                .append("success", true)
                                .toBytes()
                        :
                        new SimpleJsonObject()
                                .append("success", false)
                                .append("reason", Collections.singletonList("Maximum 15 nodes per ip allowed"))
                                .toBytes()
        );
    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException { }
}

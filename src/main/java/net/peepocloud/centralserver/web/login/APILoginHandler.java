package net.peepocloud.centralserver.web.login;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.login.APICloudData;
import net.peepocloud.centralserver.login.Account;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.util.Collections;

public class APILoginHandler extends HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getRequestHeaders();
        String username = headers.getFirst("Peepo-UserName");
        String apiToken = headers.getFirst("Peepo-ApiToken");
        if (!headers.containsKey("Peepo-UserName") || !headers.containsKey("Peepo-ApiToken")) {
            send(
                    httpExchange,
                    400,
                    new SimpleJsonObject().append("success", false).append("reason", Collections.singletonList("Missing \"Peepo-UserName\" or \"Peepo-ApiToken\" header")).toBytes()
            );
            return;
        }

        String uniqueId = null;
        if (username != null && apiToken != null) {
            Account account = CentralServer.getInstance().getLoginManager().checkAuthAPIAndGetAccount(username, apiToken);
            if (account != null) {
                for (APICloudData apiData : account.getApiData()) {
                    if (apiData.getApiToken().equals(apiToken)) {
                        uniqueId = apiData.getUniqueId();
                        break;
                    }
                }
            }
        }
        send(
                httpExchange,
                200,
                new SimpleJsonObject()
                        .append("success", true)
                        .append("response", uniqueId != null)
                        .append("uniqueId", uniqueId)
                        .toBytes()
        );
    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException { }
}

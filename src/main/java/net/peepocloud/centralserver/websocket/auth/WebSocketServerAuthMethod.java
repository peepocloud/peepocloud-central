package net.peepocloud.centralserver.websocket.auth;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.websocket.WebSocket;

public interface WebSocketServerAuthMethod {

    boolean checkAuth(WebSocket webSocket, SimpleJsonObject jsonObject);

}

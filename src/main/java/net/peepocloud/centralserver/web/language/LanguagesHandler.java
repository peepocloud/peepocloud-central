package net.peepocloud.centralserver.web.language;
/*
 * Created by Mc_Ruben on 07.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class LanguagesHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        Map<String, String> query = getQueryParameters(httpExchange);

        if (query.containsKey("name")) {
            send(
                    httpExchange, 200,
                    new SimpleJsonObject()
                            .append("success", true)
                            .append("response", CentralServer.getInstance().getLanguagesManager().getLanguageByName(query.get("name")))
                            .toBytes()
            );
        } else if (query.containsKey("shortName")) {
            send(
                    httpExchange, 200,
                    new SimpleJsonObject()
                            .append("success", true)
                            .append("response", CentralServer.getInstance().getLanguagesManager().getLanguageByShortName(query.get("shortName")))
                            .toBytes()
            );
        } else if (query.containsKey("all")) {
            send(
                    httpExchange, 200,
                    CentralServer.getInstance().getGson().toJson(CentralServer.getInstance().getLanguagesManager().getLanguages().keySet()).getBytes(StandardCharsets.UTF_8)
            );
        } else {
            send(httpExchange, 200, new SimpleJsonObject().append("success", false).append("reason", "no \"name\", \"all\" or \"shortName\" parameter was found in the query").toBytes());
        }
    }
}

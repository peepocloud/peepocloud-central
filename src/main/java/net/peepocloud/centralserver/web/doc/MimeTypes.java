package net.peepocloud.centralserver.web.doc;
/*
 * Created by Mc_Ruben on 02.11.2018
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class MimeTypes {

    private static final Map<String, String> mimeTypes = new HashMap<>();

    static {

        mimeTypes.put("css", "text/css");
        mimeTypes.put("acgi", "text/html");
        mimeTypes.put("htm", "text/html");
        mimeTypes.put("htx", "text/html");
        mimeTypes.put("html", "text/html");
        mimeTypes.put("htmls", "text/html");

    }

    public static String getMimeType(String key) {
        return mimeTypes.get(key);
    }

    public static String getType(String fileName) {
        String contentType = "text/plain";
        int indexOf = fileName.lastIndexOf('.');
        if (indexOf > 0 && fileName.length() > indexOf) {
            String end = fileName.substring(indexOf + 1);
            String mimeType = MimeTypes.getMimeType(end);
            if (mimeType != null) {
                contentType = mimeType;
            }
        }
        return contentType;
    }

}

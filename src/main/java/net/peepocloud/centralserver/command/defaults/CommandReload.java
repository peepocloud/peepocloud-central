package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

import java.io.IOException;

public class CommandReload extends Command {
    public CommandReload() {
        super("reload", null, "rl");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        try {
            CentralServer.getInstance().reload();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package net.peepocloud.centralserver;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpServer;
import jline.console.ConsoleReader;
import lombok.Getter;
import net.peepocloud.centralserver.addons.Addon;
import net.peepocloud.centralserver.addons.AddonInstaller;
import net.peepocloud.centralserver.autoupdater.AutoUpdaterManager;
import net.peepocloud.centralserver.autoupdater.UpdatesManager;
import net.peepocloud.centralserver.ban.BanManager;
import net.peepocloud.centralserver.command.CommandManager;
import net.peepocloud.centralserver.command.defaults.*;
import net.peepocloud.centralserver.discord.DiscordBot;
import net.peepocloud.centralserver.gstatistic.GStatisticsManager;
import net.peepocloud.centralserver.language.LanguagesManager;
import net.peepocloud.centralserver.logging.ColoredLogger;
import net.peepocloud.centralserver.logging.ConsoleColor;
import net.peepocloud.centralserver.login.UserLoginManager;
import net.peepocloud.centralserver.utility.Constants;
import net.peepocloud.centralserver.version.ServerVersionManager;
import net.peepocloud.centralserver.web.WebServer;
import net.peepocloud.centralserver.web.addon.AddonFileHandler;
import net.peepocloud.centralserver.web.addon.AddonHandler;
import net.peepocloud.centralserver.web.ban.AccountDeleteHandler;
import net.peepocloud.centralserver.web.ban.BanCheckHandler;
import net.peepocloud.centralserver.web.doc.DocsHandler;
import net.peepocloud.centralserver.web.language.LanguagesHandler;
import net.peepocloud.centralserver.web.login.*;
import net.peepocloud.centralserver.web.update.NodeVersionDownloadHandler;
import net.peepocloud.centralserver.web.update.UpdateCheckHandler;
import net.peepocloud.centralserver.web.update.UpdateNodeDownloadHandler;
import net.peepocloud.centralserver.web.update.UpdateZipDownloadHandler;
import net.peepocloud.centralserver.web.version.BungeeVersionsHandler;
import net.peepocloud.centralserver.web.version.SpigotVersionsHandler;
import net.peepocloud.centralserver.command.defaults.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

@Getter
public class CentralServer {
    @Getter
    private static CentralServer instance;

    private WebServer webServer;
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private ColoredLogger logger;
    private CommandManager commandManager;

    private LanguagesManager languagesManager;

    private CentralServerConfig config;

    private AddonInstaller addonInstaller = new AddonInstaller();

    private AutoUpdaterManager autoUpdaterManager;
    private UpdatesManager updatesManager;

    private ServerVersionManager serverVersionManager = new ServerVersionManager();

    private DiscordBot discordBot;

    private BanManager banManager = new BanManager("database/ban.ini");

    private UserLoginManager loginManager = new UserLoginManager("database/users.ini");

    private GStatisticsManager gStatisticsManager;

    private boolean running = true;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    CentralServer() throws IOException {
        instance = this;

        try {
            Field field = Charset.class.getDeclaredField("defaultCharset");
            field.setAccessible(true);
            field.set(null, StandardCharsets.UTF_8);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        this.logger = new ColoredLogger(new ConsoleReader(System.in, System.out));
        this.commandManager = new CommandManager(this.logger);

        this.config = CentralServerConfig.loadConfig();

        this.languagesManager = new LanguagesManager();

        this.webServer = new WebServer(this.createAddress(this.config.getWebHost(), this.config.getWebPort()));
        this.initWebHandlers();
        this.webServer.start();

        this.autoUpdaterManager = new AutoUpdaterManager(this.config.getGitCloneCommand());
        this.updatesManager = new UpdatesManager();

        this.commandManager.registerCommands(
                new CommandStop(),
                new CommandHelp(),
                new CommandAddon(),
                new CommandReload(),
                new CommandCloudUpdate(),
                new CommandBungee(),
                new CommandSpigot(),
                new CommandBan(),
                new CommandUnban(),
                new CommandCloudMiniUpdate(),
                new CommandDeleteUser()
        );

        this.discordBot = new DiscordBot("discord.json");

        this.gStatisticsManager = new GStatisticsManager("gstats.json");

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown0));

        AtomicLong currentTick = new AtomicLong(0);
        new Thread(() -> {
            while (!Thread.interrupted()) {
                if (currentTick.get() > Long.MAX_VALUE - 1000)
                    currentTick.set(0);
                this.handleTick(currentTick.incrementAndGet());
                try {
                    Thread.sleep(Constants.MILLIS_PER_TICK);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "PeepoCloud ServerTick Handler").start();
    }

    private InetSocketAddress createAddress(String host, int port) {
        if (host == null || host.equals("*"))
            return new InetSocketAddress(port);
        return new InetSocketAddress(host, port);
    }

    private void initWebHandlers() {
        this.webServer.registerHandler("/addon", new AddonHandler());
        this.webServer.registerHandler("/file", new AddonFileHandler());
        this.webServer.registerHandler("/language", new LanguagesHandler());
        this.webServer.registerHandler("/updatecheck", new UpdateCheckHandler());
        this.webServer.registerHandler("/updatenode", new UpdateNodeDownloadHandler());
        this.webServer.registerHandler("/newestVersion", new UpdateZipDownloadHandler());
        this.webServer.registerHandler("/spigotVersions", new SpigotVersionsHandler());
        this.webServer.registerHandler("/bungeeVersions", new BungeeVersionsHandler());
        this.webServer.registerHandler("/banned", new BanCheckHandler());
        this.webServer.registerHandler("/docs", new DocsHandler());
        this.webServer.registerHandler("/cloudInfoUpdate", new CloudInfoUpdateHandler());
        this.webServer.registerHandler("/apiLogin", new APILoginHandler());
        this.webServer.registerHandler("/cloudInfoAdd", new AddCloudInfoHandler());
        this.webServer.registerHandler("/createAccount", new CreateAccountHandler());
        this.webServer.registerHandler("/login", new LoginHandler());
        this.webServer.registerHandler("/delete", new AccountDeleteHandler());
        this.webServer.registerHandler("/oldVersion", new NodeVersionDownloadHandler());
    }

    private void shutdown0() {
        this.running = false;
        this.webServer.stop();

        this.discordBot.shutdown();

        try {
            this.logger.getConsoleReader().print(ConsoleColor.RESET.toString());
            this.logger.getConsoleReader().drawLine();
            this.logger.getConsoleReader().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.logger.getConsoleReader().close();
    }

    public void shutdown() {
        this.shutdown0();
        System.exit(0);
    }

    public String getDownloadUrl() {
        return this.config.getDownloadHost();
    }

    public String getDownloadUrl(String version) {
        return String.format(this.config.getVersionDownloadHost(), version);
    }

    public Collection<Addon> getAddons() {
        return this.config.getAddons();
    }

    public Addon getAddon(String name, String version) {
        for (Addon addon : getAddons()) {
            if (addon.getName().equalsIgnoreCase(name) && addon.getVersion().equalsIgnoreCase(version)) {
                return addon;
            }
        }
        return null;
    }

    public void reload() throws IOException {
        String oldHost = this.config.getWebHost();
        int oldPort = this.config.getWebPort();
        this.config.reload();

        this.autoUpdaterManager.setCloneCommand(this.config.getGitCloneCommand());

        if (!Objects.equals(oldHost, this.config.getWebHost()) || !Objects.equals(oldPort, this.config.getWebPort())) {
            this.webServer.stop();
            this.webServer = new WebServer(this.createAddress(this.config.getWebHost(), this.config.getWebPort()));
            this.initWebHandlers();
            this.webServer.start();
        }

        this.gStatisticsManager.reload();

        this.languagesManager.reload();
    }

    public void handleTick(long currentTick) {
        this.discordBot.handleTick(currentTick);
        if (currentTick % 600 == 0) {
            this.gStatisticsManager.save();
            this.loginManager.save();
        }
    }

}

package net.peepocloud.centralserver.discord;
/*
 * Created by Mc_Ruben on 25.11.2018
 */

import lombok.Getter;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommandManager;
import net.peepocloud.centralserver.discord.command.defaults.*;
import net.peepocloud.centralserver.discord.listener.DiscordUserInputListener;
import net.peepocloud.centralserver.discord.support.SupportManager;
import net.peepocloud.centralserver.discord.userinput.UserTextInput;

import javax.security.auth.login.LoginException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Getter
public class DiscordBot {

    private DiscordBotConfig config;
    private JDA jda;
    private Guild guild;
    private TextChannel updateChannel;

    private DiscordCommandManager commandManager;

    private SupportManager supportManager = new SupportManager("tmp/discord.tmp");

    private Collection<UserTextInput> textInputs = new ArrayList<>();

    public DiscordBot(String configPath) {
        this.config = new DiscordBotConfig(configPath);
        if (this.config.getToken() == null) {
            System.out.println("&cPlease fill the discord bot config at &e" + configPath);
            return;
        }

        this.startJDA(aBoolean -> {
            if (aBoolean) {
                System.out.println("&aSuccessfully started the JDA");
            } else {
                System.err.println("&cThere was an error while starting up the JDA");
            }
        });
    }

    public void reload(Consumer<Boolean> success) {
        System.out.println("&eTrying to reload the Bot...");
        String oldToken = this.config.getToken();
        this.config.load();
        if (!Objects.equals(oldToken, this.config.getToken()) && this.config.getToken() != null) {
            if (this.jda != null) {
                this.jda.shutdownNow();
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.startJDA(aBoolean -> {
                if (aBoolean) {
                    System.out.println("&aSuccessfully reloaded the Bot");
                } else {
                    System.err.println("&cThere was an error while starting up the JDA");
                }
                success.accept(aBoolean);
            });
        } else {
            this.guild = this.jda.getGuildById(this.config.getGuildId());
            this.updateChannel = this.guild.getTextChannelById(this.config.getUpdateChannelId());
            this.supportManager.load(this);
            success.accept(true);
            System.out.println("&aSuccessfully reloaded the Bot");
        }
    }

    public void shutdown() {
        if (this.jda != null)
            this.jda.shutdownNow();
    }

    public void sendRandomGame() {
        Game game = this.config.getRandomGame(this.jda);
        if (game != null) {
            this.jda.getPresence().setGame(game);
        }
    }

    public void requestTextInput(long memberId, long channelId, Consumer<Message> consumer) {
        this.textInputs.add(new UserTextInput(memberId, channelId, consumer, -1, null));
    }

    public void requestTextInput(long memberId, long channelId, Consumer<Message> consumer, long expireMillis, Runnable expired) {
        this.textInputs.add(new UserTextInput(memberId, channelId, consumer, System.currentTimeMillis() + expireMillis, expired));
    }

    public void handleTick(long currentTick) {
        if (currentTick % 5 == 0 && !this.textInputs.isEmpty()) {
            Collection<UserTextInput> remove = new LinkedList<>();
            for (UserTextInput textInput : this.textInputs) {
                if (textInput.getExpireMillis() != -1 && System.currentTimeMillis() >= textInput.getExpireMillis()) {
                    remove.add(textInput);
                }
            }
            if (!remove.isEmpty()) {
                this.textInputs.removeAll(remove);
            }
        }

        this.supportManager.handleTick(currentTick);
    }

    private void startJDA(Consumer<Boolean> success) {
        System.out.println("&eTrying to startup JDA...");
        try {
            this.jda = new JDABuilder(AccountType.BOT)
                    .setToken(this.config.getToken())
                    .setGame(this.config.getRandomGame(null))
                    .addEventListener(new ListenerAdapter() {
                        @Override
                        public void onReady(ReadyEvent event) {
                            jda.removeEventListener(this);

                            guild = event.getJDA().getGuildById(config.getGuildId());
                            if (guild == null) {
                                System.out.println("&cNo guild with the id &e" + config.getGuildId() + " &cwas found");
                                if (success != null) {
                                    success.accept(false);
                                }
                                return;
                            }

                            supportManager.load(DiscordBot.this);

                            updateChannel = guild.getTextChannelById(config.getUpdateChannelId());
                            if (updateChannel == null) {
                                System.out.println("&cNo update channel with the id &e" + config.getUpdateChannelId() + " &cwas found on the guild &e" + guild.getName());
                            }

                            if (success != null) {
                                success.accept(true);
                            }

                            new Thread(() -> {
                                while (!Thread.interrupted()) {
                                    try {
                                        Thread.sleep(20000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    sendRandomGame();
                                }
                            }, "JDA presence updater").start();
                        }
                    })
                    .setStatus(OnlineStatus.ONLINE)
                    .setAutoReconnect(true)
                    .setAudioEnabled(false)

                    .addEventListener(new DiscordUserInputListener())

                    .build();

            this.commandManager = new DiscordCommandManager(this.jda)

                    .registerCommands(
                            new CommandVersion(),
                            new CommandReload(),
                            new CommandCloudUpdate(),
                            new CommandForceSupportEnd(),
                            new CommandSupportEnd(),
                            new CommandGStats(),
                            new CommandCloudMiniUpdate(),
                            new CommandVersions(),
                            new CommandDeleteUser()
                    );
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

    public void doAutoUpdate(Message message, Member member, String oldVersion, String newVersion) {
        CentralServer.getInstance().getExecutorService().execute(() -> {
            message.delete().queue();

            message.getChannel().sendMessage("Should there be a message in the update channel on discord? (Then write \"yes\")").queue(message3 -> {
                CentralServer.getInstance().getDiscordBot().requestTextInput(member.getUser().getIdLong(), message.getChannel().getIdLong(), sendDiscordMessage -> {
                    message3.delete().queue();
                    sendDiscordMessage.delete().queue();

                    String a = sendDiscordMessage.getContentRaw();
                    boolean discordMessage = a.equals("true") || a.equals("yes") || a.equals("ja");
                    if (discordMessage) {
                        message.getChannel().sendMessage("Type in the text for discord").queue(message1 -> {
                            message1.delete().queueAfter(3, TimeUnit.SECONDS);
                            CentralServer.getInstance().getDiscordBot().requestTextInput(member.getUser().getIdLong(), message.getChannel().getIdLong(), responseMessage -> {
                                message.getChannel().sendMessage("Updating to \"" + newVersion + "\"...").queue(message2 -> {
                                    responseMessage.delete().queue();
                                    String discordText = responseMessage.getContentDisplay();
                                    CentralServer.getInstance().getAutoUpdaterManager().doUpdate(
                                            newVersion,
                                            discordText,
                                            onSuccess(message, message2, oldVersion, newVersion),
                                            true
                                    );
                                });
                            });
                        });
                    } else {
                        message.getChannel().sendMessage("Updating to \"" + newVersion + "\"...").queue(message2 -> {
                            CentralServer.getInstance().getAutoUpdaterManager().doUpdate(
                                    newVersion,
                                    null,
                                    onSuccess(message, message2, oldVersion, newVersion),
                                    false
                            );
                        });
                    }
                });
            });
        });
    }

    private Consumer<Boolean> onSuccess(Message message, Message message2, String oldVersion, String newVersion) {
        return success -> {
            if (success) {
                message2.delete().queue();
                message.getChannel().sendMessage("Successfully updated the system from \"" + oldVersion + "\" to \"" + newVersion + "\"")
                        .queue(message1 -> message1.delete().queueAfter(3, TimeUnit.SECONDS));
            } else {
                message2.delete().queue();
                message.getChannel().sendMessage("An error occurred while attempting to update the system to \"" + newVersion + "\", see the logs for more information")
                        .queue(message1 -> message1.delete().queueAfter(3, TimeUnit.SECONDS));
            }
        };
    }

}

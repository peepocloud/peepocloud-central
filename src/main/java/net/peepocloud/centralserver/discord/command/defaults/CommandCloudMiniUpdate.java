package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 08.11.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.discord.command.DiscordCommandPermission;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class CommandCloudMiniUpdate extends DiscordCommand {
    public CommandCloudMiniUpdate() {
        super("cloudminiupdate", DiscordCommandPermission.DEVEOPER);
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        String version = CentralServer.getInstance().getUpdatesManager().getNewestVersion();

        CentralServer.getInstance().getDiscordBot().doAutoUpdate(message, member, version, version);
    }


}

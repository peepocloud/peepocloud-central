package net.peepocloud.centralserver.discord.support;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.DiscordBot;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfo;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfosManager;
import net.peepocloud.centralserver.utility.Constants;
import net.peepocloud.centralserver.utility.ObjectSerialization;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Getter
public class SupportManager {

    private final Path tmpFileMessages;
    private final Path tmpFileUsers;

    private PeepoCloudInfosManager cloudInfosManager = new PeepoCloudInfosManager();

    private TextChannel supportChannelDE;
    private TextChannel supportChannelEN;

    private TextChannel supportRequestChannelDE;
    private TextChannel supportRequestChannelEN;

    private Role supportingRoleDE;
    private Role supportingRoleEN;


    private ListenerImpl listener;
    private Map<Long, Long> messages = new HashMap<>(); //UserId, TimeOut for Request
    private Map<Long, SupportingUser> supportingUsers = new HashMap<>(); //UserId, Lang

    private int lastTmpSaveAmountMessages = 0;
    private int lastTmpSaveAmountUsers = 0;

    private Collection<Long> waitingForUnique = new HashSet<>();

    public SupportManager(String tmpFile) {
        this.tmpFileMessages = Paths.get(tmpFile + ".messages");
        this.tmpFileUsers = Paths.get(tmpFile + ".users");

        if (Files.exists(this.tmpFileMessages)) {
            Object object = ObjectSerialization.deserialize(this.tmpFileMessages);
            if (object != null) {
                this.messages = (Map<Long, Long>) object;
                this.lastTmpSaveAmountMessages = this.messages.size();
            }
        }
        if (Files.exists(this.tmpFileUsers)) {
            Object object = ObjectSerialization.deserialize(this.tmpFileUsers);
            if (object != null) {
                this.supportingUsers = (Map<Long, SupportingUser>) object;
                this.lastTmpSaveAmountUsers = this.supportingUsers.size();
            }
        }
    }

    public void load(DiscordBot bot) {
        this.supportChannelDE = bot.getGuild().getTextChannelById(bot.getConfig().getSupportChannelIdDE());
        this.supportRequestChannelDE = bot.getGuild().getTextChannelById(bot.getConfig().getSupportRequestChannelIdDE());
        this.supportingRoleDE = bot.getGuild().getRoleById(bot.getConfig().getSupportingRoleIdDE());

        this.supportChannelEN = bot.getGuild().getTextChannelById(bot.getConfig().getSupportChannelIdEN());
        this.supportRequestChannelEN = bot.getGuild().getTextChannelById(bot.getConfig().getSupportRequestChannelIdEN());
        this.supportingRoleEN = bot.getGuild().getRoleById(bot.getConfig().getSupportingRoleIdEN());

        if (this.supportChannelDE == null) {
            System.err.println("&cNo Discord support channel DE with the id &e" + bot.getConfig().getSupportChannelIdDE() + " &cwas found, disabling support...");
        } else if (this.supportRequestChannelDE == null) {
            System.err.println("&cNo Discord support request channel DE with the id &e" + bot.getConfig().getSupportRequestChannelIdDE() + " &cwas found, disabling support...");
        } else if (this.supportingRoleDE == null) {
            System.err.println("&cNo Discord supporting role DE with the id &e" + bot.getConfig().getSupportingRoleIdDE() + " &cwas found, disabling support...");
        } else if (this.supportChannelEN == null) {
            System.err.println("&cNo Discord support channel EN with the id &e" + bot.getConfig().getSupportChannelIdDE() + " &cwas found, disabling support...");
        } else if (this.supportRequestChannelEN == null) {
            System.err.println("&cNo Discord support request channel EN with the id &e" + bot.getConfig().getSupportRequestChannelIdDE() + " &cwas found, disabling support...");
        } else if (this.supportingRoleEN == null) {
            System.err.println("&cNo Discord supporting role EN with the id &e" + bot.getConfig().getSupportingRoleIdDE() + " &cwas found, disabling support...");
        }
        boolean enable = this.supportChannelDE != null && this.supportRequestChannelDE != null && this.supportingRoleDE != null &&
                this.supportChannelEN != null && this.supportRequestChannelEN != null && this.supportingRoleEN != null;
        if (this.listener != null) {
            bot.getJda().removeEventListener(this.listener);
        }
        if (enable) {
            this.listener = new ListenerImpl();
            bot.getJda().addEventListener(this.listener);
        }
    }

    public void startSupport(Member user, PeepoCloudInfo cloudInfo, Message startMessage, String lang) {
        String contents = startMessage.getContentRaw();
        startMessage.delete().queue();
        user.getGuild().getController().addSingleRoleToMember(user, lang.equals("DE") ? this.supportingRoleDE : this.supportingRoleEN).queue();
        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setTitle("**" + (user.getNickname() == null ? user.getUser().getName() : user.getNickname() + (" (" + user.getUser().getName() + ")")) + "**")
                .setDescription(contents);

        if (cloudInfo.getOs() != null) {
            embedBuilder.addField("Operating-System: ", cloudInfo.getOs(), false);
        }
        if (cloudInfo.getCloudVersion() != null) {
            embedBuilder.addField("Cloud-Version: ", cloudInfo.getCloudVersion(), false);
        }
        if (cloudInfo.getIsp() != null) {
            embedBuilder.addField("ISP: ", cloudInfo.getIsp(), false);
        }
        embedBuilder.addField("Max-Memory: ", cloudInfo.getMaxMemory() + " MB", false);

        (lang.equals("DE") ? this.supportChannelDE : this.supportChannelEN)
                .sendMessage(
                                embedBuilder
                                .setAuthor(lang.equals("DE") ? "Nutze \"!supportEnd\" um den Support zu beenden" : "Use \"!supportEnd\" to close the support conversation")
                                .setTimestamp(Instant.now())

                                .build()
                ).queue(message -> this.supportingUsers.put(user.getUser().getIdLong(), new SupportingUser(lang, message.getIdLong())));
    }

    public void handleTick(long currentTick) {
        if (currentTick % 10 == 0 && !this.messages.isEmpty()) {
            long time = System.currentTimeMillis();
            new HashMap<>(this.messages).forEach((userId, ratelimitTimeout) -> {
                if (time > ratelimitTimeout) {
                    this.messages.remove(userId);
                }
            });
        }

        if (currentTick % 5 == 0) {
            if (this.lastTmpSaveAmountMessages != this.messages.size()) {
                ObjectSerialization.serialize(this.messages, this.tmpFileMessages);
                this.lastTmpSaveAmountMessages = this.messages.size();
            }
            if (this.lastTmpSaveAmountUsers != this.supportingUsers.size()) {
                ObjectSerialization.serialize(this.supportingUsers, this.tmpFileUsers);
                this.lastTmpSaveAmountUsers = this.supportingUsers.size();
            }
        }
    }

    public boolean handleSupportEndRequest(Member user) {
        if (this.supportingUsers.containsKey(user.getUser().getIdLong())) {
            SupportingUser supportingUser = this.supportingUsers.remove(user.getUser().getIdLong());
            user.getGuild().getController().removeSingleRoleFromMember(user, supportingUser.lang.equals("DE") ? this.supportingRoleDE : this.supportingRoleEN).queue();
            TextChannel channel = supportingUser.lang.equals("DE") ? this.supportChannelDE : this.supportChannelEN;
            channel.getMessageById(supportingUser.messageId).queue(message -> {
                if (message != null) {
                    String desc = null;
                    Collection<MessageEmbed.Field> fields = Collections.emptyList();
                    for (MessageEmbed embed : message.getEmbeds()) {
                        fields = embed.getFields();
                        desc = embed.getDescription();
                    }
                    if (desc == null)
                        desc = supportingUser.lang.equals("DE") ? "Dieses Gespräch wurde beendet" : "This support conversation has been closed";
                    else
                        desc = (supportingUser.lang.equals("DE") ? "Dieses Gespräch wurde beendet\n" : "This support conversation has been closed\n") + desc;

                    EmbedBuilder builder = new EmbedBuilder()
                            .setTitle("**" + (user.getNickname() == null ? user.getUser().getName() : user.getNickname() + (" (" + user.getUser().getName() + ")")) + "**")
                            .setDescription(desc);

                    for (MessageEmbed.Field field : fields) {
                        builder.addField(field);
                    }
                    message.editMessage(builder.build()).queue();
                }
            }, throwable -> {});
            return true;
        }
        return false;
    }

    private final class ListenerImpl extends ListenerAdapter {
        @Override
        public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
            if (event.getAuthor().isBot())
                return;
            String lang = event.getChannel().getIdLong() == supportRequestChannelDE.getIdLong() ?
                    "DE" : event.getChannel().getIdLong() == supportRequestChannelEN.getIdLong() ? "EN" : null;
            if (lang != null && !waitingForUnique.contains(event.getAuthor().getIdLong())) {
                if (messages.containsKey(event.getAuthor().getIdLong())) {
                    event.getChannel().sendMessage(lang.equals("DE") ?
                            "Bitte warte einige Sekunden zwischen Fragen, um Spam zu vermeiden!"
                            : "Please wait a few seconds between questions to prevent spam!")

                            .queue(message -> {
                                        message.delete().queueAfter(8, TimeUnit.SECONDS,
                                                aVoid -> event.getMessage().delete().queue(),
                                                throwable -> event.getMessage().delete().queue()
                                        );
                                        event.getMessage().delete().queue();
                                    }
                            );
                    return;
                }

                event.getChannel().sendMessage(
                        lang.equals("DE") ?
                                "Bitte schreibe die UniqueId deiner Cloud, diese findest du, wenn du \"unique\" in der Konsole deiner Node eingibst " + event.getAuthor().getAsMention() + " (Diese Anfrage wird nach 30 Sekunden gelöscht)" :
                                "Please write the unique id of your cloud, you can find it by typing \"unique\" into the console of your Node " + event.getAuthor().getAsMention() + " (This request will be deleted after 30 seconds)"
                )
                        .queue(requestMessage -> {
                            waitingForUnique.add(event.getAuthor().getIdLong());
                            CentralServer.getInstance().getDiscordBot().requestTextInput(
                                    event.getAuthor().getIdLong(),
                                    event.getChannel().getIdLong(),
                                    response -> {
                                        requestMessage.delete().queue();
                                        response.delete().queue();
                                        waitingForUnique.remove(event.getAuthor().getIdLong());

                                        String uniqueId = response.getContentDisplay();

                                        PeepoCloudInfo cloudInfo = cloudInfosManager.getInfo(uniqueId);
                                        if (cloudInfo == null) {
                                            event.getChannel().sendMessage(
                                                    lang.equals("DE") ?
                                                            "Es wurde keine Info über eine Cloud mit dieser UniqueId gefunden. Bitte gib \"supportupdate\" in einer deiner Nodes ein und versuche es noch einmal, wenn es immer noch nicht funktioniert, ist deine UniqueId falsch." :
                                                            "We couldn't find information about a cloud with that unique id. Please type \"supportupdate\" in one of your Nodes and try it again, if it still not works, your unique id is incorrect."
                                                    )
                                                    .queue(message -> {
                                                        event.getMessage().delete().queueAfter(10, TimeUnit.SECONDS, aVoid -> {
                                                            message.delete().queue();
                                                        });
                                                    });
                                            return;
                                        }

                                        startSupport(
                                                event.getMember(),
                                                cloudInfo,
                                                event.getMessage(),
                                                lang
                                        );

                                        messages.put(event.getAuthor().getIdLong(), System.currentTimeMillis() + Constants.MILLIS_SUPPORT_RATELIMIT);
                                    },
                                    30000,
                                    () -> {
                                        waitingForUnique.remove(event.getAuthor().getIdLong());
                                        requestMessage.delete().queue();
                                    }
                            );
                        });

            }
        }
    }

    @AllArgsConstructor
    private static final class SupportingUser implements Serializable {
        private String lang;
        private long messageId;
    }

}

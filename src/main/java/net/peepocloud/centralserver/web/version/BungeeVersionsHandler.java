package net.peepocloud.centralserver.web.version;
/*
 * Created by Mc_Ruben on 24.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class BungeeVersionsHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        send(httpExchange, 200,
                CentralServer.getInstance().getGson().toJson(CentralServer.getInstance().getServerVersionManager().getBungeeVersionParents()).getBytes(StandardCharsets.UTF_8)
        );
    }
}

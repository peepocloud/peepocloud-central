package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 01.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.json.SimpleJsonObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CommandVersion extends DiscordCommand {
    private static final DateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy-kk:mm");

    public CommandVersion() {
        super("version");
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        String version = args.length == 0 ? CentralServer.getInstance().getUpdatesManager().getNewestVersion() : args[0];
        if (!CentralServer.getInstance().getUpdatesManager().getVersions().contains(version)) {
            message.getChannel().sendMessage("The version \"" + version + "\" does not exist").queue();
            return;
        }
        SimpleJsonObject jsonObject = CentralServer.getInstance().getUpdatesManager().loadVersionInfo(version);
        String s = args.length == 0 ?
                "The newest version of PeepoCloud is currently **" + (version == null ? "not available" : version) + "**" :
                "**" + version + "**";

        if (jsonObject.contains("startTime"))
            s += ", it has been built at " + FORMAT.format(jsonObject.getLong("startTime"));
        if (jsonObject.contains("lines") && jsonObject.contains("filesAmount"))
            s += ", lines in the java source " + jsonObject.getLong("lines") + " in " + jsonObject.getLong("filesAmount") + " files";
        if (version != null)
            s += ", download: " + (args.length == 0 ? CentralServer.getInstance().getDownloadUrl() : CentralServer.getInstance().getDownloadUrl(version));
        message.getChannel().sendMessage(s).queue();
    }
}

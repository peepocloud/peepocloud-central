package net.peepocloud.centralserver.discord.presence;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import lombok.*;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import net.peepocloud.centralserver.CentralServer;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class DiscordConfigGame {
    private String type;
    private String text;

    private transient Game game;

    public Game toGame(JDA jda) {
        if (this.game == null) {
            try {
                this.game = Game.of(Game.GameType.valueOf(this.type), this.text
                        .replace("%users%", String.valueOf(jda == null ? "0" : jda.getUsers().stream().filter(user -> !user.isBot()).count()))
                        .replace("%serversStarted%", String.valueOf(CentralServer.getInstance().getGStatisticsManager().getServerStarts()))
                        .replace("%bungeesStarted%", String.valueOf(CentralServer.getInstance().getGStatisticsManager().getBungeeStarts()))
                        .replace("%nodesConnected%", String.valueOf(CentralServer.getInstance().getGStatisticsManager().getNodeConnects()))
                );
            } catch (Exception e) {
            }
        }
        return this.game;
    }
}

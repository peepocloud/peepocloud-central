package net.peepocloud.centralserver.discord.listener;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.peepocloud.centralserver.CentralServer;

import java.util.ArrayList;

public class DiscordUserInputListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!CentralServer.getInstance().getDiscordBot().getTextInputs().isEmpty()) {
            new ArrayList<>(CentralServer.getInstance().getDiscordBot().getTextInputs()).stream()
                    .filter(userTextInput -> userTextInput.getMemberId() == event.getAuthor().getIdLong() &&
                            userTextInput.getChannelId() == event.getChannel().getIdLong())
                    .forEach(userTextInput -> {
                        userTextInput.getConsumer().accept(event.getMessage());
                        CentralServer.getInstance().getDiscordBot().getTextInputs().remove(userTextInput);
            });
        }
    }
}

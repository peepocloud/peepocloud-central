package net.peepocloud.centralserver.web.update;
/*
 * Created by Mc_Ruben on 10.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class UpdateZipDownloadHandler extends HttpHandler {
    @Override
    public void handle1(String hashedIp, HttpExchange httpExchange) throws IOException {
        String version = CentralServer.getInstance().getUpdatesManager().getNewestVersion();
        try (InputStream inputStream = CentralServer.getInstance().getUpdatesManager().getVersionAsStream(version);
             OutputStream outputStream = httpExchange.getResponseBody()) {
            httpExchange.getResponseHeaders().set("Content-Disposition", "filename='PeepoCloud-" + version + ".zip");
            httpExchange.sendResponseHeaders(200, inputStream.available());
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
        }
    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
    }
}

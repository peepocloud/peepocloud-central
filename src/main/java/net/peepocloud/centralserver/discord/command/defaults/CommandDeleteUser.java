package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 23.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.ban.Ban;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.discord.command.DiscordCommandPermission;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfo;
import net.peepocloud.centralserver.utility.Hashing;

import java.util.concurrent.TimeUnit;

public class CommandDeleteUser extends DiscordCommand {

    public CommandDeleteUser() {
        super("deleteuser", DiscordCommandPermission.ADMIN, "delete");
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        message.delete().queue();

        if (args.length != 2) {
            message.getChannel()
                    .sendMessage("delete IP|UNIQUE <ip/uniqueId>")
                    .queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));;
            return;
        }

        if (args[0].equalsIgnoreCase("ip")) {
            String ip = args[1];
            if (CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByIp(Hashing.hashString(ip))) {
                message.getChannel()
                        .sendMessage("Successfully removed the cloud info for the ip \"" + ip + "\" out of the memory")
                        .queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));
                CentralServer.getInstance().getBanManager().ban(new Ban(ip, Ban.Type.IP, "Deleted"));
            } else {
                message.getChannel().sendMessage("No cloud info for the ip \"" + ip + "\" was found in the memory")
                        .queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));
            }
        } else if (args[0].equalsIgnoreCase("unique")) {
            String uniqueId = args[1];
            PeepoCloudInfo info = CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().getInfo(uniqueId);
            if (CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByUniqueId(uniqueId)) {
                CentralServer.getInstance().getBanManager().banWithHashedIp(info.getHashedIpAddress(), "Deleted");
                CentralServer.getInstance().getLoginManager().delete(info.getUniqueId());
                message.getChannel()
                        .sendMessage("Successfully removed the cloud info for the uniqueId \"" + uniqueId + "\" out of the memory")
                        .queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));
            } else {
                message.getChannel()
                        .sendMessage("No cloud info for the uniqueId \"" + uniqueId + "\" was found in the memory")
                        .queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));
            }
        } else {
            message.getChannel().sendMessage("delete IP|UNIQUE <ip/uniqueId>").queue(message1 -> message1.delete().queueAfter(5, TimeUnit.SECONDS));
        }
    }
}

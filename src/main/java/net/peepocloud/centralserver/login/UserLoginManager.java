package net.peepocloud.centralserver.login;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import lombok.*;
import net.peepocloud.centralserver.utility.Hashing;
import net.peepocloud.centralserver.utility.ObjectSerialization;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Getter
@ToString
@EqualsAndHashCode
public class UserLoginManager {

    private final Path file;

    private Map<String, Account> accounts = new HashMap<>();

    public UserLoginManager(String file) {
        this.file = Paths.get(file);
        this.load();
    }

    public void load() {
        Object object = ObjectSerialization.deserialize(this.file);
        if (object != null)
            this.accounts = (Map<String, Account>) object;
    }

    public void save() {
        ObjectSerialization.serialize(this.accounts, this.file);
    }

    /**
     * Checks if a user with the given {@code username} exists and if the given {@code password} is correct
     *
     * @param username the user name of the {@link Account}
     * @param password the password of the {@link Account}
     * @return {@code true} if an {@link Account} with that user name exists and the given password is equal to it or {@code false} if not
     */
    public boolean checkAuth(String username, String password) {
        if (!this.accounts.containsKey(username))
            return false;
        return this.accounts.get(username).checkAuth(username, password);
    }

    /**
     * Checks if a user with the given {@code username} exists and if the given {@code password} is correct
     *
     * @param username the user name of the {@link Account}
     * @param password the password of the {@link Account}
     * @return the {@link Account} if it exists with that user name and the given password is equal to it or {@code null} if not
     */
    public Account checkAuthAndGetAccount(String username, String password) {
        if (!this.accounts.containsKey(username))
            return null;
        Account account = this.accounts.get(username);
        if (!account.checkAuth(username, password))
            return null;
        return account;
    }

    /**
     * Checks if a user with the given {@code username} exists and if the given {@code apiToken} is correct
     *
     * @param username the user name of the {@link Account}
     * @param uniqueId one uniqueId of the {@link Account}
     * @param apiToken the api token of the {@link APICloudData} in the {@link Account} with the given {@code uniqueId}
     * @return {@code true} if an {@link Account} with that user name exists and the given api token is equal to it or {@code false} if not
     */
    public boolean checkAuthAPI(String username, String uniqueId, String apiToken) {
        if (!this.accounts.containsKey(username))
            return false;
        return this.accounts.get(username).checkAuthAPI(username, uniqueId, apiToken);
    }

    /**
     * Checks if a user with the given {@code username} exists and if the given {@code apiToken} is correct
     *
     * @param username the user name of the {@link Account}
     * @param apiToken the api token of the {@link APICloudData} in the {@link Account}
     * @return {@code true} if an {@link Account} with that user name exists and the given api token is equal to it or {@code false} if not
     */
    public boolean checkAuthAPI(String username, String apiToken) {
        if (!this.accounts.containsKey(username))
            return false;
        return this.accounts.get(username).checkAuthAPI(username, apiToken);
    }

    /**
     * Checks if a user with the given {@code username} exists and if the given {@code apiToken} is correct
     *
     * @param username the user name of the {@link Account}
     * @param apiToken the api token of the {@link APICloudData} in the {@link Account}
     * @return the {@link Account} if it exists with that user name and the given api token is equal to it or {@code null} if not
     */
    public Account checkAuthAPIAndGetAccount(String username, String apiToken) {
        if (!this.accounts.containsKey(username))
            return null;
        Account account = this.accounts.get(username);
        if (!account.checkAuthAPI(username, apiToken))
            return null;
        return account;
    }

    /**
     * Creates a new {@link Account} to login in from the web site
     *
     * @param username the user name of the new account
     * @param password the password of the new account
     * @return a new {@link Account} or if an account with that username already exists {@code null}
     */
    public Account register(String username, String password) {
        if (this.accounts.containsKey(username))
            return null;
        Account account = new Account(username, Hashing.hashString(password), new ArrayList<>());
        this.accounts.put(username, account);
        return account;
    }

    public boolean delete(String uniqueId) {
        Account account = null;
        for (Account value : this.accounts.values()) {
            for (APICloudData apiData : value.getApiData()) {
                if (apiData.getUniqueId().equals(uniqueId)) {
                    account = value;
                    break;
                }
            }
            if (account != null) {
                break;
            }
        }
        if (account != null) {
            this.accounts.remove(account.getUsername(), account);
            this.save();
        }
        return account != null;
    }

    public boolean deleteByUsername(String username) {
        Account account = this.accounts.get(username);
        if (account == null)
            return false;
        this.accounts.remove(username, account);
        this.save();
        return true;
    }

}

package net.peepocloud.centralserver.logging;

import lombok.Getter;
import org.fusesource.jansi.Ansi;

import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

@Getter
public enum ConsoleColor {

    RESET("reset", 'r', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.DEFAULT).boldOff().toString()),

    GREEN("green", 'a', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.GREEN).bold().toString()),
    LIGHT_BLUE("light_blue", 'b', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.CYAN).bold().toString()),
    RED("red", 'c', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.RED).bold().toString()),
    YELLOW("yellow", 'e', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.YELLOW).bold().toString()),
    WHITE("white", 'f', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.WHITE).bold().toString()),

    BLACK("black", '0', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLACK).boldOff().toString()),
    DARK_BLUE("dark_blue", '1', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLUE).boldOff().toString()),
    DARK_GREEN("dark_green", '2', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.GREEN).boldOff().toString()),
    CYAN("cyan", '3', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.CYAN).boldOff().toString()),
    DARK_RED("dark_red", '4', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.RED).boldOff().toString()),
    PURPLE("purple", '5', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.MAGENTA).boldOff().toString()),
    ORANGE("orange", '6', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.YELLOW).boldOff().toString()),
    DARK_GRAY("dark_gray", '8', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLACK).bold().toString()),
    GRAY("gray", '7', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.WHITE).boldOff().toString()),
    BLUE("blue", '9', Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLUE).bold().toString());

    public static final char COLOR_CHAR = '&';
    private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("[&][a-f0-9r]");

    private final String name;
    private String ansiCode;

    private final char index;

    ConsoleColor(String name, char index, String ansiCode) {
        this.name = name;
        this.index = index;
        this.ansiCode = ansiCode;
    }

    public static String stripColor(String input) {
        return STRIP_COLOR_PATTERN.matcher(input.replaceAll("\u001B\\[[;\\d]*m", "")).replaceAll("");
    }

    public static String toColouredString(String text) {
        if (text == null)
            throw new NullPointerException("text");

        for (ConsoleColor consoleColor : values())
            text = text.replace(COLOR_CHAR + "" + consoleColor.index, consoleColor.ansiCode);

        return text;
    }

    public static String toRandomColourString(String text) {
        if (text == null) throw new NullPointerException();

        char[] chars = text.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (char aChar : chars)
            builder.append(randomColour().ansiCode).append(aChar);

        return builder.toString();
    }

    public static ConsoleColor randomColour() {
        ConsoleColor[] colours = values();
        return colours[ThreadLocalRandom.current().nextInt(colours.length)];
    }

    @Override
    public String toString() {
        return ansiCode;
    }
}

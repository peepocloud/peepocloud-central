package net.peepocloud.centralserver.autoupdater;
/*
 * Created by Mc_Ruben on 08.11.2018
 */

import com.google.gson.reflect.TypeToken;
import lombok.AllArgsConstructor;
import lombok.Setter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.addons.AddonInstaller;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.utility.SystemUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.shared.invoker.*;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class AutoUpdaterManager {

    private static final DateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy-HH.mm.ss");

    @Setter
    private String cloneCommand;
    private boolean disableLogging = false;

    private Collection<Module> modules = Arrays.asList(new FileCopyModule());

    private MavenXpp3Reader reader = new MavenXpp3Reader();
    private MavenXpp3Writer writer = new MavenXpp3Writer();

    public AutoUpdaterManager(String cloneCommand) {
        this.cloneCommand = cloneCommand;
    }

    private File cloneRepository(String cloneCommand, File directory) {
        System.out.println("Cloning from git repository &e" + cloneCommand);
        try {
            Process process = new ProcessBuilder(cloneCommand.split(" ")).directory(directory).inheritIO().start();
            /*try (InputStream inputStream = process.getInputStream(); //wait until it is cloned
                 InputStreamReader reader = new InputStreamReader(inputStream);
                 BufferedReader bufferedReader = new BufferedReader(reader)) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
            }*/
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            SystemUtils.sleepUninterruptedly(700);

            process.destroyForcibly();

            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    return file;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return directory;
    }

    public void doUpdate(String newVersion, String discordText, Consumer<Boolean> consumer, boolean sendDiscord) {
        CentralServer.getInstance().getExecutorService().execute(() -> {
            long buildStart = System.currentTimeMillis();

            int id = ThreadLocalRandom.current().nextInt(100000) + 10000;
            File directory = new File("tmp/update-" + newVersion + "-" + id);
            System.out.println("&eTrying to build version &e" + newVersion + " in directory &e" + directory.toString());

            directory.mkdirs();

            directory = this.cloneRepository(this.cloneCommand, directory);

            Path pom = Paths.get(directory.toString(), "pom.xml");
            this.replaceVersion(pom, newVersion);

            SimpleJsonObject jsonObject = SimpleJsonObject.load(directory.toString() + "/.projectProperties.json");
            if (!jsonObject.contains("nodePath")) {
                consumer.accept(false);
                deleteDirectory(directory);
                System.out.println("&cCould not update, there was no &e.projectProperties.json &cfound or it did not contain &e\"nodePath\"");
                return;
            }

            BuildProperties buildProperties = jsonObject.getObject("build", BuildProperties.class);
            if (buildProperties == null || buildProperties.goals == null) {
                consumer.accept(false);
                deleteDirectory(directory);
                System.out.println("&cCould not update, the &e.projectProperties.json &cdid not contain a &ebuildentry or the build object did not contain a &egoalsobject which is needed for build");
                return;
            }

            if (buildProperties.removablePlugins != null && !buildProperties.removablePlugins.isEmpty()) {
                for (RemovablePlugin removablePlugin : buildProperties.removablePlugins) {
                    this.removePlugin(directory.toString(), removablePlugin);
                }
            }

            List<String> currentGoals = null;
            try {

                for (Goal goal : buildProperties.goals) {
                    currentGoals = goal.goals;
                    if (goal.doBefore != null) {
                        for (Action action : goal.doBefore) {
                            action.call(this.modules, directory.toString());
                        }
                    }
                    if (!this.doMavenPackaging(directory, id, newVersion, goal.goals)) {
                        consumer.accept(false);
                        deleteDirectory(directory);
                        return;
                    }
                    if (goal.doAfter != null) {
                        for (Action action : goal.doAfter) {
                            action.call(this.modules, directory.toString());
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println("&cMaven package of version &e" + newVersion + " &cfailed for goals &e" + currentGoals + "&c, see logs &e(\"maven-logs/" + newVersion + "-" + id + ".log\") &cfor more information");
                e.printStackTrace();
                consumer.accept(false);
                deleteDirectory(directory);
                return;
            }

            File template = new File(directory, ".template");

            Path outputZip = Paths.get("releases/" + newVersion + ".zip");
            Path outputNode = Paths.get("releases/" + newVersion + "-node.jar");
            if (!Files.exists(outputZip.getParent())) {
                try {
                    Files.createDirectory(outputZip.getParent());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Path node = Paths.get(directory.toString() + "/" + jsonObject.getString("nodePath"));
            if (!Files.exists(node)) {
                consumer.accept(false);
                deleteDirectory(directory);
                System.out.println("&cCould not update, the path \"" + node.toString() + "\" does not exist");
                return;
            }

            try {
                Files.copy(node, outputNode);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (template.exists() && template.isDirectory()) {
                {
                    if (jsonObject.contains("addons")) {
                        Collection<AddonModule> modules = (Collection<AddonModule>) jsonObject.getObject("addons", new TypeToken<Collection<AddonModule>>() {
                        }.getType());
                        if (modules != null) {
                            for (AddonModule module : modules) {
                                Path path = Paths.get(directory.toString(), module.getPath());
                                if (!Files.exists(path)) {
                                    System.out.println("&cFile " + module.getPath() + " does not exist for addon " + module.getName());
                                    continue;
                                }
                                try {
                                    long start = System.nanoTime();
                                    AddonInstaller.AddonInstallResult result = CentralServer.getInstance().getAddonInstaller().addAddon(
                                            module.getName(), module.getVersion(), module.getVersions(), module.getAuthors(), path
                                    );
                                    System.out.println(result.formatMessage(System.nanoTime() - start));
                                } catch (Exception e) {
                                    System.out.println("&cError while trying to install addon " + module.getName() + " from the last build");
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

                try {
                    Files.deleteIfExists(outputZip);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try (OutputStream outputStream = Files.newOutputStream(outputZip, StandardOpenOption.CREATE_NEW);
                     ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
                    fillTemplate(template.toPath(), directory.toPath(), zipOutputStream);

                    CentralServer.getInstance().getUpdatesManager().addVersion(newVersion);

                    this.copyDocs(directory.toString(), jsonObject, newVersion);

                    this.createInfo(directory.toString(), jsonObject, System.currentTimeMillis() - buildStart, newVersion, Paths.get("releases/" + newVersion + ".json"));

                    consumer.accept(true);
                    deleteDirectory(directory);
                    System.out.println("&aSuccessfully created version " + newVersion + " in &e\"releases/" + newVersion + ".zip\"");
                    if (sendDiscord) {
                        this.sendDiscordText(newVersion, discordText);
                    }
                } catch (IOException e) {
                    try {
                        Files.delete(outputNode);
                        Files.deleteIfExists(outputZip);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    System.out.println("&cFailed to create version, an internal error occured");
                    e.printStackTrace();
                    consumer.accept(false);
                    deleteDirectory(directory);
                }
            } else {
                if (!Files.exists(outputZip)) {
                    try {
                        Files.createFile(outputZip);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                this.copyDocs(directory.toString(), jsonObject, newVersion);

                this.createInfo(directory.toString(), jsonObject, System.currentTimeMillis() - buildStart, newVersion, Paths.get("releases/" + newVersion + ".json"));

                CentralServer.getInstance().getUpdatesManager().addVersion(newVersion);
                System.out.println("&aSuccessfully created version " + newVersion + " in &e\"releases/" + newVersion + ".jar\" &cWITHOUT A ZIP");
                consumer.accept(true);
                deleteDirectory(directory);
                if (sendDiscord) {
                    this.sendDiscordText(newVersion, discordText);
                }
            }

        });
    }

    private void fillTemplate(Path template, Path directory, ZipOutputStream zipOutputStream) throws IOException {
        Files.walkFileTree(
                template,
                EnumSet.noneOf(FileVisitOption.class),
                Integer.MAX_VALUE,
                new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        try {
                            if (file.getFileName().toString().equalsIgnoreCase(".file-copy")) {
                                SimpleJsonObject jsonObject = SimpleJsonObject.load(file);
                                if (jsonObject.contains("sourcePath") && jsonObject.contains("targetName")) {
                                    Path source = Paths.get(directory.toString(), jsonObject.getString("sourcePath"));
                                    if (Files.exists(source)) {
                                        Path target = Paths.get(file.getParent().toString() + "/" + jsonObject.getString("targetName"));
                                        if (!Files.isDirectory(target)) {
                                            if (Files.exists(target)) {
                                                Files.delete(target);
                                            }

                                            Files.copy(source, target);

                                            zipOutputStream.putNextEntry(new ZipEntry(template.relativize(target).toString()));

                                            Files.copy(target, zipOutputStream);
                                        }
                                    }
                                }
                            } else {
                                zipOutputStream.putNextEntry(new ZipEntry(template.relativize(file).toString()));
                                Files.copy(file, zipOutputStream);
                            }

                            zipOutputStream.closeEntry();
                        } catch (Exception e) {
                            e.printStackTrace();
                            zipOutputStream.closeEntry();
                        }

                        return FileVisitResult.CONTINUE;
                    }
                }
        );
    }

    private boolean doMavenPackaging(File directory, int id, String buildName, List<String> goals) throws IOException {
        if (disableLogging) {
            return this.doPackage(directory, goals, s -> {
            }) == 0;
        }

        Path logFile = Paths.get("maven-logs/" + buildName + "-" + id + ".log");
        Files.deleteIfExists(logFile);

        if (!Files.exists(logFile.getParent()))
            Files.createDirectories(logFile.getParent());
        Files.createFile(logFile);

        OutputStream outputStream = Files.newOutputStream(logFile);
        PrintWriter printWriter = new PrintWriter(outputStream);

        int result = this.doPackage(directory, goals, s -> printWriter.println("[" + FORMAT.format(new Date()) + "] " + s));

        printWriter.close();
        outputStream.close();
        return result == 0;
    }

    private int doPackage(File directory, List<String> goals, Consumer<String> lineHandler) {
        this.delombok(directory.toString());

        int exitCode = -1;

        Invoker invoker = new DefaultInvoker();
        if (System.getProperties().containsKey("update.mavenhome")) {
            invoker.setMavenHome(new File(System.getProperty("update.mavenhome")));
        }
        invoker.setOutputHandler(lineHandler::accept);
        try {
            InvocationRequest request = new DefaultInvocationRequest();
            request.setBatchMode(true);
            request.setPomFile(new File(directory + "/pom.xml"));
            request.setBaseDirectory(directory);
            request.setGoals(goals);

            System.out.println("[MAVEN] Building Project " + directory + "...");
            InvocationResult result = invoker.execute(request);
            exitCode = result.getExitCode();
        } catch (MavenInvocationException e) {
            e.printStackTrace();
        }
        System.out.println("&7Maven project exited with exitCode &e" + exitCode);
        return exitCode;
    }

    private void deleteDirectory(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                    file.delete();
                } else {
                    file.delete();
                }
            }
        }
        directory.delete();
    }

    private void removePlugin(String buildDirectory, RemovablePlugin plugin) {
        Path pom = Paths.get(buildDirectory, plugin.pom);
        if (!Files.exists(pom)) {
            System.err.println("&cPom for removableplugin &e" + plugin.groupId + "-" + plugin.artifactId + " &cin buildDirectory &e" + buildDirectory + " &cdoes not exist");
            return;
        }
        try (InputStream inputStream = Files.newInputStream(pom)) {
            Model model = this.reader.read(inputStream);
            model.getBuild().getPlugins().stream()
                    .filter(mavenPlugin ->
                            mavenPlugin.getGroupId().equals(plugin.groupId) &&
                                    mavenPlugin.getArtifactId().equals(plugin.artifactId) &&
                                    (plugin.version == null || mavenPlugin.getVersion().equals(plugin.version)))
                    .collect(Collectors.toList()).forEach(plugin1 -> model.getBuild().getPlugins().remove(plugin1));
            try (OutputStream outputStream = Files.newOutputStream(pom)) {
                this.writer.write(outputStream, model);
            }
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private void replaceVersion(Path pomFile, String version) {
        Path tmpPom = Paths.get(pomFile.getParent().toString(), "tmpPom.xml");
        try {
            Files.copy(pomFile, tmpPom, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStream inputStream = Files.newInputStream(tmpPom);
             OutputStream outputStream = Files.newOutputStream(pomFile)) {

            Model model = reader.read(inputStream);
            model.getProperties().setProperty("cloud.version", version);
            writer.write(outputStream, model);

            Files.delete(tmpPom);
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private void sendDiscordText(String version, String text) {
        if (CentralServer.getInstance().getDiscordBot().getUpdateChannel() != null) {
            MessageEmbed messageEmbed = new EmbedBuilder()
                    .setTitle("**» UPDATE " + version + "**")
                    .setImage("https://cdn.discordapp.com/emojis/402867832175460363.png?v=1")

                    .setDescription(text)

                    .build();
            CentralServer.getInstance().getDiscordBot().getUpdateChannel().sendMessage(messageEmbed).queue();
            //CentralServer.getInstance().getDiscordBot().getUpdateChannel().sendMessage("@everyone").queue();
        }
    }

    private void delombok(String directory) {
        try {
            if (!Files.exists(Paths.get("tmp/lombok.jar"))) {
                System.out.println("&eDownloading lombok...");
                URLConnection connection = new URL("http://central.maven.org/maven2/org/projectlombok/lombok/1.18.4/lombok-1.18.4.jar").openConnection();
                connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                connection.setConnectTimeout(1000);
                connection.connect();
                try (InputStream inputStream = connection.getInputStream()) {
                    Files.copy(inputStream, Paths.get("tmp/lombok.jar"));
                    System.out.println("&aSuccessfully downloaded lombok");
                }
            }

            System.out.println("&eTrying to delombok " + directory + "...");

            String targetDirectory = "tmp/" + UUID.randomUUID();

            String[] commands = new String[]{
                    "java", "-jar", "tmp/lombok.jar", "delombok", directory, "--target=" + targetDirectory
            };
            Process process = new ProcessBuilder(commands).inheritIO().start();
            try {
                if (!process.waitFor(1, TimeUnit.MINUTES)) {
                    process.destroyForcibly();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.copyDirectory(Paths.get(targetDirectory), directory);
            this.deleteDirectory(new File(targetDirectory));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyDocs(String buildDir, SimpleJsonObject jsonObject, String version) {
        Map<String, String> paths = (Map<String, String>) jsonObject.getObject("docs", new TypeToken<Map<String, String>>() {
        }.getType());
        if (paths == null)
            return;
        paths.forEach((name, dir) -> {
            this.copyDocs(buildDir, name, dir, version);
        });
    }

    private void copyDocs(String buildDir, String name, String dir, String version) {
        if (name == null || dir == null) {
            return;
        }

        Path source = Paths.get(buildDir, dir);
        if (!Files.exists(source))
            return;

        String target = "docs/" + version + "/" + name;
        if (!Files.exists(Paths.get(target))) {
            try {
                Files.createDirectories(Paths.get(target));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.copyDirectory(source, target);
    }

    private void createInfo(String buildDir, SimpleJsonObject nodeInfo, long timeTakenForBuild, String version, Path outputFile) {
        SimpleJsonObject jsonObject = new SimpleJsonObject();

        AtomicLong a = new AtomicLong();
        AtomicLong b = new AtomicLong();

        System.out.println("Counting files/lines in project &e" + buildDir + "&7...");
        try {
            Files.walkFileTree(
                    Paths.get(buildDir),
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (file.toString().endsWith(".java")) {
                                Stream stream = Files.lines(file);
                                a.addAndGet(stream.count());
                                stream.close();
                                b.incrementAndGet();
                            }
                            return FileVisitResult.CONTINUE;
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        long lines = a.get();
        long filesAmount = b.get();
        System.out.println("&aSuccessfully counted &e" + lines + " lines &ain &e" + filesAmount + " files &ain project &e" + buildDir);

        jsonObject.append("lines", lines)
                .append("filesAmount", filesAmount)
                .append("version", version)
                .append("startTime", System.currentTimeMillis())
                .append("time", timeTakenForBuild)
                .append("nodeInfo", nodeInfo.asJsonObject());

        jsonObject.saveAsFile(outputFile);

    }

    private void copyDirectory(Path directory, String targetDirectory) {
        if (!Files.exists(directory))
            return;
        try {
            Files.walkFileTree(
                    directory,
                    EnumSet.noneOf(FileVisitOption.class),
                    Integer.MAX_VALUE,
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            Path target = Paths.get(targetDirectory, directory.relativize(file).toString());
                            Path parent = target.getParent();
                            if (parent != null && !Files.exists(parent))
                                Files.createDirectories(parent);
                            Files.copy(file, target, StandardCopyOption.REPLACE_EXISTING);
                            return FileVisitResult.CONTINUE;
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AllArgsConstructor
    private static class Goal {
        private List<String> goals;
        private Collection<Action> doBefore, doAfter;
    }

    @AllArgsConstructor
    private static class Action {
        private String module;
        private Map<String, String> options;

        public void call(Collection<Module> modules, String buildDirectory) {
            Module module = modules.stream().filter(mod -> mod.getName().equals(this.module)).findFirst().orElse(null);
            if (module == null) {
                System.err.println("&cModule &e" + this.module + " &cwas not found. Available: &e" + modules.stream().map(Module::getName).collect(Collectors.joining(", ")));
                return;
            }
            module.call(buildDirectory, this.options);
        }
    }

    @AllArgsConstructor
    private static class BuildProperties {
        private Collection<Goal> goals;
        private Collection<RemovablePlugin> removablePlugins;
    }

    @AllArgsConstructor
    private static class RemovablePlugin {
        private String pom;
        private String groupId;
        private String artifactId;
        private String version; //optional
    }

    private static abstract class Module {
        public abstract String getName();

        public abstract void call(String buildDirectory, Map<String, String> options);
    }

    private static class FileCopyModule extends Module {

        @Override
        public String getName() {
            return "file-copy";
        }

        @Override
        public void call(String buildDirectory, Map<String, String> options) {
            if (!options.containsKey("from") || !options.containsKey("to")) {
                System.err.println("&cOptions " + options + " did not contain \"from\" or \"to\" parameters which are needed for the &efile-copy module");
                return;
            }
            Path path = Paths.get(buildDirectory, options.get("from"));
            if (!Files.exists(path)) {
                System.err.println("&cFile &e" + options.get("from") + " (from) &cwas not found in buildDirectory &e" + buildDirectory);
                return;
            }

            Path target = Paths.get(buildDirectory, options.get("to"));
            if (!Files.exists(target)) {
                Path parent = target.getParent();
                if (parent != null && !Files.exists(parent)) {
                    try {
                        Files.createDirectories(parent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (Files.isDirectory(path)) {
                SystemUtils.copyDirectory(path, target.toString());
            } else {
                try {
                    Files.copy(path, target, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

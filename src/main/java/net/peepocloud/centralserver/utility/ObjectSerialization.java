package net.peepocloud.centralserver.utility;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class ObjectSerialization {

    private ObjectSerialization() { }

    public static byte[] serialize(Object object) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        serialize(object, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static void serialize(Object object, OutputStream outputStream) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void serialize(Object object, Path path) {
        if (!Files.exists(path)) {
            Path parent = path.getParent();
            try {
                if (parent != null && !Files.exists(parent)) {
                    Files.createDirectories(parent);
                }
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (OutputStream outputStream = Files.newOutputStream(path)) {
            serialize(object, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object deserialize(byte[] bytes) {
        return deserialize(new ByteArrayInputStream(bytes));
    }

    public static Object deserialize(InputStream inputStream) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object deserialize(Path path) {
        if (!Files.exists(path))
            return null;
        try (InputStream inputStream = Files.newInputStream(path)) {
            return deserialize(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

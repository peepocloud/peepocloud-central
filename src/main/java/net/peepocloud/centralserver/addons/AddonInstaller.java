package net.peepocloud.centralserver.addons;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import lombok.AllArgsConstructor;
import net.md_5.bungee.http.HttpClient;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.utility.SystemUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.ThreadLocalRandom;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class AddonInstaller {

    public boolean validateAddon(Path path) {
        System.out.println("&eValidating addon &9" + path.getFileName().toString());
        long start = System.nanoTime();
        try (JarFile jarFile = new JarFile(path.toFile())) {
            JarEntry jarEntry = jarFile.getJarEntry("addon.yml");
            System.out.println("&aSuccessfully validated addon &9" + path.getFileName().toString() + "&a, took &c" + (System.nanoTime() - start) + "ns");
            jarFile.close();
            return jarEntry != null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public AddonInstallResult addAddon(String name, String version, String[] allVersions, String[] authors, InputStream inputStream) throws IOException {
        Path tmp = Paths.get("tmp/" + name + "-" + version + "_" + (ThreadLocalRandom.current().nextInt(1000000) + 10000));
        Path parent = tmp.getParent();
        if (parent != null && !Files.exists(parent)) {
            Files.createDirectories(parent);
        }
        Files.copy(inputStream, tmp, StandardCopyOption.REPLACE_EXISTING);

        if (Files.size(tmp) > 100000000) {
            Files.delete(tmp);
            return AddonInstallResult.FILE_TOO_BIG;
        }

        if (!validateAddon(tmp)) {
            Files.delete(tmp);
            return AddonInstallResult.MISSING_ADDON_YML;
        }

        Path path = Paths.get("addon/" + name + "-" + version + ".peepocloud.addon");

        Files.deleteIfExists(path);
        Path addons = Paths.get("addon");
        if (!Files.exists(addons)) {
            Files.createDirectory(addons);
        }
        Files.copy(tmp, path);
        Files.delete(tmp);

        Addon addon = new Addon(
                name,
                authors,
                version,
                allVersions,
                path
        );
        CentralServer.getInstance().getConfig().getAddons().add(
                addon
        );
        CentralServer.getInstance().getConfig().save();

        return AddonInstallResult.SUCCESS;
    }

    public AddonInstallResult addAddon(String name, String version, String[] allVersions, String[] authors, Path path) throws IOException {
        if (!Files.exists(path))
            return AddonInstallResult.INVALID_URL;
        try (InputStream inputStream = Files.newInputStream(path)) {
            return addAddon(name, version, allVersions, authors, inputStream);
        }
    }

    public AddonInstallResult addAddon(String name, String version, String[] allVersions, String[] authors, String downloadUrl) throws IOException {
        URLConnection connection = null;
        try {
            connection = new URL(downloadUrl).openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.setConnectTimeout(5000);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.connect();
        } catch (Exception e) {
            e.printStackTrace();
            return AddonInstallResult.INVALID_URL;
        }
        try (InputStream inputStream = connection.getInputStream()) {
            return addAddon(name, version, allVersions, authors, inputStream);
        }
    }

    @AllArgsConstructor
    public enum AddonInstallResult {
        SUCCESS("&aSuccessfully downloaded and installed the addon, took &c%sns"),
        INVALID_URL("&cThe specified url was not valid and we couldn't download any files from there"),
        FILE_TOO_BIG("&cThe size of the specified file was too big, max. 100 MB"),
        MISSING_ADDON_YML("&cThe file downloaded did not contain an addon.yml");

        private final String message;

        public String formatMessage(long time) {
            if (this == SUCCESS)
                return String.format(this.message, Long.toString(time));
            return this.message;
        }
    }

}

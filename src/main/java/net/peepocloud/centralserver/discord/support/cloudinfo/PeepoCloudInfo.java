package net.peepocloud.centralserver.discord.support.cloudinfo;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import lombok.*;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class PeepoCloudInfo {
    private String uniqueId;
    private String hashedIpAddress;
    private String isp;
    private String os;
    private String cloudVersion;
    private int maxMemory;
    private int cpuCores;
    private transient long lastUpdate;
}

package net.peepocloud.centralserver.version;
/*
 * Created by Mc_Ruben on 24.11.2018
 */

import com.google.gson.reflect.TypeToken;
import lombok.*;
import net.peepocloud.centralserver.json.SimpleJsonObject;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public class ServerVersionManager {

    private Path configPath = Paths.get("serverVersions.json");
    private Map<String, ServerVersionParent> spigotVersionParents = new HashMap<>();
    private Map<String, ServerVersionParent> bungeeVersionParents = new HashMap<>();

    public ServerVersionManager() {
        this.load();
    }

    public boolean addSpigotVersion(String name, String version, String url) {
        if (!this.spigotVersionParents.containsKey(name))
            this.addSpigotParent(name);
        ServerVersionParent parent = this.spigotVersionParents.get(name);
        if (parent.getVersions().containsKey(version))
            return false;
        parent.getVersions().put(version, new ServerVersion(version, url));
        this.save();
        return true;
    }

    public boolean removeSpigotVersion(String name, String version) {
        if (!this.spigotVersionParents.containsKey(name))
            return false;
        ServerVersionParent parent = this.spigotVersionParents.get(name);
        if (!parent.getVersions().containsKey(version))
            return false;
        parent.getVersions().remove(version);
        this.save();
        return true;
    }

    public boolean addSpigotParent(String name, ServerVersion... versions) {
        if (this.spigotVersionParents.containsKey(name))
            return false;
        ServerVersionParent parent = new ServerVersionParent(name, Arrays.stream(versions).collect(Collectors.toMap(ServerVersion::getVersion, o -> o)));
        this.spigotVersionParents.put(parent.getName(), parent);
        this.save();
        return true;
    }

    public boolean removeSpigotParent(String name) {
        if (!this.spigotVersionParents.containsKey(name))
            return false;
        this.spigotVersionParents.remove(name);
        this.save();
        return true;
    }


    public boolean addBungeeVersion(String name, String version, String url) {
        if (!this.bungeeVersionParents.containsKey(name))
            this.addBungeeParent(name);
        ServerVersionParent parent = this.bungeeVersionParents.get(name);
        if (parent.getVersions().containsKey(version))
            return false;
        parent.getVersions().put(version, new ServerVersion(version, url));
        this.save();
        return true;
    }

    public boolean removeBungeeVersion(String name, String version) {
        if (!this.bungeeVersionParents.containsKey(name))
            return false;
        ServerVersionParent parent = this.bungeeVersionParents.get(name);
        if (!parent.getVersions().containsKey(version))
            return false;
        parent.getVersions().remove(version);
        this.save();
        return true;
    }

    public boolean addBungeeParent(String name, ServerVersion... versions) {
        if (this.bungeeVersionParents.containsKey(name))
            return false;
        ServerVersionParent parent = new ServerVersionParent(name, Arrays.stream(versions).collect(Collectors.toMap(ServerVersion::getVersion, o -> o)));
        this.bungeeVersionParents.put(parent.getName(), parent);
        this.save();
        return true;
    }

    public boolean removeBungeeParent(String name) {
        if (!this.bungeeVersionParents.containsKey(name))
            return false;
        this.bungeeVersionParents.remove(name);
        this.save();
        return true;
    }

    public void save() {
        new SimpleJsonObject()
                .append("spigot", this.spigotVersionParents)
                .append("bungee", this.bungeeVersionParents)
                .saveAsFile(this.configPath);
    }

    public void load() {
        SimpleJsonObject jsonObject = SimpleJsonObject.load(this.configPath);
        this.spigotVersionParents = (Map<String, ServerVersionParent>) jsonObject.getObject("spigot", new TypeToken<Map<String, ServerVersionParent>>() {
        }.getType());
        this.bungeeVersionParents = (Map<String, ServerVersionParent>) jsonObject.getObject("bungee", new TypeToken<Map<String, ServerVersionParent>>() {
        }.getType());

        if (this.spigotVersionParents == null)
            this.spigotVersionParents = new HashMap<>();
        if (this.bungeeVersionParents == null)
            this.bungeeVersionParents = new HashMap<>();
    }

}

package net.peepocloud.centralserver.autoupdater;
/*
 * Created by Mc_Ruben on 10.11.2018
 */

import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import net.peepocloud.centralserver.json.SimpleJsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UpdatesManager {

    @Getter
    private List<String> versions = new ArrayList<>();

    public UpdatesManager() {
        SimpleJsonObject jsonObject = SimpleJsonObject.load("allVersions.json");
        if (jsonObject.contains("version")) {
            this.versions = (List<String>) jsonObject.getObject("version", new TypeToken<List<String>>() {
            }.getType());
        }
    }

    public boolean addVersion(String version) {
        if (this.versions.contains(version))
            return false;
        this.versions.add(version);
        new SimpleJsonObject().append("version", this.versions).saveAsFile("allVersions.json");
        return true;
    }

    public String getNewestVersion() {
        return this.versions.isEmpty() ? null : this.versions.get(this.versions.size() - 1);
    }

    public InputStream getVersionAsStream(String version) {
        Path path = Paths.get("releases/" + version + ".zip");
        if (!Files.exists(path)) {
            return null;
        }
        try {
            return Files.newInputStream(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream getNewestVersionAsStream() {
        String version = getNewestVersion();
        if (version == null)
            return null;
        return this.getVersionAsStream(version);
    }

    public InputStream getNewestVersionNodeAsStream() {
        String version = getNewestVersion();
        if (version == null)
            return null;
        Path path = Paths.get("releases/" + version + "-node.jar");
        if (!Files.exists(path)) {
            return null;
        }
        try {
            return Files.newInputStream(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getVersionsBehind(String version) {
        if (!versions.contains(version))
            return versions.size();
        int versionsBehind = 0;
        Iterator<String> iterator = this.versions.iterator();
        do {
            versionsBehind++;
        } while (iterator.hasNext() && iterator.next().equalsIgnoreCase(version));
        return versionsBehind - 1;
    }

    public boolean isUpdateAvailable(String version) {
        int versionsBehind = getVersionsBehind(version);
        return versionsBehind != 0;
    }

    public SimpleJsonObject loadVersionInfo(String version) {
        return SimpleJsonObject.load("releases/" + version + ".json");
    }

}

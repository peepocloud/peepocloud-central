package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 04.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

public class CommandStop extends Command {
    public CommandStop() {
        super("stop");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        CentralServer.getInstance().shutdown();
    }
}

package net.peepocloud.centralserver.addons;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import lombok.*;

import java.io.Serializable;
import java.nio.file.Path;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Addon implements Serializable {
    private String name;
    private String[] authors;
    private String version;
    private String[] allVersions;
    private transient Path path;
}

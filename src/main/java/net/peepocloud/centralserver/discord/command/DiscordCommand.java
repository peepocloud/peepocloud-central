package net.peepocloud.centralserver.discord.command;
/*
 * Created by Mc_Ruben on 01.12.2018
 */

import lombok.Getter;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

@Getter
public abstract class DiscordCommand {

    private String name;
    private DiscordCommandPermission permission;
    private String[] aliases;

    public DiscordCommand(String name, DiscordCommandPermission permission, String... aliases) {
        this.name = name;
        this.permission = permission;
        this.aliases = aliases;
    }

    public DiscordCommand(String name, String... aliases) {
        this(name, DiscordCommandPermission.USER, aliases);
    }

    public abstract void execute(Member member, Message message, String[] args);

}

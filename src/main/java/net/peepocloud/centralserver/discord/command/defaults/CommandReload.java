package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.discord.command.DiscordCommandPermission;

public class CommandReload extends DiscordCommand {
    public CommandReload() {
        super("reload", DiscordCommandPermission.ADMIN, "rl");
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        CentralServer.getInstance().getDiscordBot().reload(success -> message.getChannel().sendMessage(
                success ?
                        "Successfully reloaded the Bot" :
                        "There was an error while trying to reload the Bot, see logs for more information").queue()
        );
    }
}

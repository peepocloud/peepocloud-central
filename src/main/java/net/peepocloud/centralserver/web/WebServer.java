package net.peepocloud.centralserver.web;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import com.sun.net.httpserver.HttpServer;
import lombok.Getter;
import net.peepocloud.centralserver.utility.SystemUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class WebServer {

    private static final long RATELIMIT_TIME_MILLIS = 1000;

    @Getter
    private HttpServer httpServer;
    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    private boolean stopped = false;

    private Map<String, Long> rateLimits = new ConcurrentHashMap<>();

    public WebServer(InetSocketAddress inetSocketAddress) throws IOException {
        this.httpServer = HttpServer.create(inetSocketAddress, 0);
    }

    public void start() {
        this.httpServer.start();

        this.executorService.execute(() -> {
            while (!Thread.interrupted() && !stopped) {
                if (!this.rateLimits.isEmpty()) {
                    new HashMap<>(this.rateLimits).forEach((hashedIp, lastConnect) -> {
                        if (System.currentTimeMillis() > lastConnect + RATELIMIT_TIME_MILLIS) {
                            this.rateLimits.remove(hashedIp);
                        }
                    });
                    SystemUtils.sleepUninterruptedly(2000);
                }
            }
        });
    }

    public void stop() {
        this.stopped = true;
        this.httpServer.stop(0);
        this.executorService.shutdownNow();
    }

    public void registerHandler(String path, HttpHandler handler) {
        handler.server = this;
        this.httpServer.createContext(path, handler);
    }

    public void createRateLimit(String hashedIp) {
        this.rateLimits.put(hashedIp, System.currentTimeMillis());
    }

    public boolean isRateLimited(String hashedIp) {
        if (!this.rateLimits.containsKey(hashedIp))
            return false;
        long lastConnect = this.rateLimits.get(hashedIp);
        boolean a = System.currentTimeMillis() > lastConnect + RATELIMIT_TIME_MILLIS;
        if (!a) {
            this.rateLimits.remove(hashedIp);
        }
        return a;
    }

}

package net.peepocloud.centralserver.web.update;
/*
 * Created by Mc_Ruben on 23.12.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class NodeVersionDownloadHandler extends HttpHandler {
    @Override
    public void handle1(String hashedIp, HttpExchange httpExchange) throws IOException {
        Map<String, String> query = getQueryParameters(httpExchange);
        String version;
        if (query.containsKey("version")) {
            version = query.get("version");
        } else {
            version = CentralServer.getInstance().getUpdatesManager().getNewestVersion();
        }

        InputStream inputStream = CentralServer.getInstance().getUpdatesManager().getVersionAsStream(version);

        if (inputStream == null) {
            httpExchange.getResponseHeaders().set("Content-Type", "text/html");
            send(
                    httpExchange,
                    200,
                    "<h4>This version does not exist</h4>".getBytes(StandardCharsets.UTF_8)
            );
            return;
        }

        httpExchange.getResponseHeaders().set("Content-Disposition", "filename='PeepoCloud-" + version + ".zip");
        httpExchange.sendResponseHeaders(200, inputStream.available());
        OutputStream outputStream = httpExchange.getResponseBody();
        byte[] buf = new byte[1024];
        int len;
        while ((len = inputStream.read(buf)) != -1) {
            outputStream.write(buf, 0, len);
        }
        outputStream.close();

        inputStream.close();

    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException { }
}

package net.peepocloud.centralserver.gstatistic;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.ban.Ban;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfo;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.websocket.WebSocketServer;
import net.peepocloud.centralserver.websocket.auth.SimpleUserTokenAuthMethod;

import java.net.InetAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class GStatisticsManager {

    private final Path file;

    private WebSocketServer webSocketServer;
    private String host;
    private int port;

    private SimpleJsonObject statistics;

    public GStatisticsManager(String file) {
        this.file = Paths.get(file);
        this.statistics = SimpleJsonObject.load(this.file);
        this.reload();
    }

    public void reload() {
        SimpleJsonObject jsonObject = SimpleJsonObject.load(this.file);
        String host = jsonObject.contains("host") ? jsonObject.getString("host") : this.getLocalHost();
        int port = jsonObject.contains("port") ? jsonObject.getInt("port") : 1351;
        if (!jsonObject.contains("port")) {
            jsonObject.append("port", port)
                    .saveAsFile(this.file);
        }
        if (!Objects.equals(host, this.host) || port != this.port) {
            if (this.webSocketServer != null)
                this.webSocketServer.close();
            this.webSocketServer = new WebSocketServer(new SimpleUserTokenAuthMethod());
            if (host.equals("*")) {
                this.webSocketServer.bind(port);
            } else {
                this.webSocketServer.bind(host, port);
            }
        }
        this.host = host;
        this.port = port;
    }

    public void save() {
        this.statistics.saveAsFile(this.file);
    }

    public void handleCloudInput(String ip, String key, long val) {
        if (key == null)
            return;
        if (val < 0) {
            CentralServer.getInstance().getBanManager().ban(new Ban(ip, Ban.Type.IP, "Tried to add " + val + " to the global statistics and manipulate them"));
            return;
        }
        if (key.equals("cloudOnlineTime")) {
            if (val > System.currentTimeMillis()) {
                CentralServer.getInstance().getBanManager().ban(new Ban(ip, Ban.Type.IP, "Tried to add " + val + " to the global statistics and manipulate them"));
                return;
            }
        }



    }

    public long getServerStarts() {
        return a("serverStarts");
    }

    public long getBungeeStarts() {
        return a("bungeeStarts");
    }

    public long getNodeConnects() {
        return a("nodeConnects");
    }

    private long a(String key) {
        if (this.statistics.contains(key))
            return this.statistics.getLong(key);
        return 0;
    }

    private String getLocalHost() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            return "*";
        }
    }

}

package net.peepocloud.centralserver;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import lombok.Getter;
import net.peepocloud.centralserver.addons.Addon;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.utility.ObjectSerialization;

import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

@Getter
public class CentralServerConfig {

    private static final Path ADDON_PATH = Paths.get("database/addons.ini");
    private static final String PATH = "data.json";

    private transient Collection<Addon> addons;
    private String gitCloneCommand;
    private String downloadHost;
    private String versionDownloadHost;
    private String webHost;
    private int webPort;

    public static CentralServerConfig loadConfig() {
        CentralServerConfig config = new CentralServerConfig();
        config.reload();
        return config;
    }

    public void reload() {
        try {
            this.addons = (Collection<Addon>) ObjectSerialization.deserialize(ADDON_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.addons == null)
            this.addons = new ArrayList<>();
        SimpleJsonObject jsonObject = SimpleJsonObject.load(PATH);
        if (!Files.exists(Paths.get(PATH))) {
            jsonObject.append("webHost", "*").append("webPort", 1350).append("gitCloneCommand", "git clone <repo>").append("downloadHost", "http://localhost:1350/newestVersion");
            jsonObject.saveAsFile(PATH);
        }
        this.webHost = jsonObject.contains("webHost") ? jsonObject.getString("webHost") : this.getLocalHost();
        this.webPort = jsonObject.contains("webPort") ? jsonObject.getInt("webPort") : 1350;
        this.gitCloneCommand = jsonObject.getString("gitCloneCommand");
        this.downloadHost = jsonObject.getString("downloadHost");
        this.versionDownloadHost = jsonObject.getString("versionDownloadHost");
        this.updateAddonPaths();
    }

    private String getLocalHost() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            return "*";
        }
    }

    public void save() {
        new SimpleJsonObject(SimpleJsonObject.GSON.toJsonTree(this).getAsJsonObject()).saveAsFile(PATH);
        try {
            ObjectSerialization.serialize(this.addons, ADDON_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateAddonPaths() {
        this.addons.forEach(addon -> {
            addon.setPath(Paths.get("addon/" + addon.getName() + "-" + addon.getVersion() + ".peepocloud.addon"));
        });
    }

}

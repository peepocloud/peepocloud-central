package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.discord.command.DiscordCommandPermission;

import java.util.concurrent.TimeUnit;

public class CommandForceSupportEnd extends DiscordCommand {
    public CommandForceSupportEnd() {
        super("fsupportend", DiscordCommandPermission.MODERATOR, "fse");
    }

    @Override
    public void execute(Member mod, Message message, String[] args) {
        if (args.length != 1) {
            message.getChannel().sendMessage("Please use \"!fsupportend <User>\"").queue(message1 -> {
                message.delete().queue();
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
            });
            return;
        }

        Member member = null;
        if (!message.getMentionedMembers().isEmpty()) {
            member = message.getMentionedMembers().get(0);
        } else {
            long id = -1;
            try {
                id = Long.parseLong(args[0]);
            } catch (Exception e) {
            }
            member = mod.getGuild().getMemberById(id);
        }
        if (member == null) {
            message.getChannel().sendMessage("This member was not found").queue(message1 -> {
                message.delete().queue();
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
            });
            return;
        }
        if (CentralServer.getInstance().getDiscordBot().getSupportManager().handleSupportEndRequest(member)) {
            message.getChannel().sendMessage("The support conversation by **" + member.getUser().getName() + "** has been closed").queue(message1 -> {
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
                message.delete().queue();
            });
        } else {
            message.getChannel().sendMessage("**" + member.getUser().getName() + "** doesn't have any support conversations at this moment").queue(message1 -> {
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
                message.delete().queue();
            });
        }
    }
}

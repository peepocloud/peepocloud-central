package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 24.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

public class CommandBungee extends Command {
    public CommandBungee() {
        super("bungee");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length == 0) {
            this.sendHelp(sender);
            return;
        }

        switch (args[0].toLowerCase()) {
            case "add": {
                if (args.length == 4) {
                    if (CentralServer.getInstance().getServerVersionManager().addBungeeVersion(args[1], args[2], args[3])) {
                        sender.sendMessage("&aSuccessfully added version &e" + args[2] + " &afor parent &e" + args[1] + " &awith the url &e" + args[3]);
                    } else {
                        sender.sendMessage("&cA version with the name &e" + args[2] + " &cfor parent &e" + args[1] + " &calready exists");
                    }
                } else {
                    this.sendHelp(sender);
                }
            }
            break;

            case "remove": {
                switch (args.length) {
                    case 3: {
                        if (CentralServer.getInstance().getServerVersionManager().removeBungeeVersion(args[1], args[2])) {
                            sender.sendMessage("&aSuccessfully removed spigot version &e" + args[2] + " &afor parent &e" + args[1]);
                        } else {
                            sender.sendMessage("&cNo spigot version &e" + args[2] + " &cfor parent &e" + args[1] + " &cwas found");
                        }
                    }
                    break;

                    case 2: {
                        if (CentralServer.getInstance().getServerVersionManager().removeBungeeParent(args[1])) {
                            sender.sendMessage("&aSuccessfully removed spigot parent version &e" + args[1]);
                        } else {
                            sender.sendMessage("&cNo spigot parent version with the name &e" + args[1] + " &cwas found");
                        }
                    }
                    break;

                    default: {
                        this.sendHelp(sender);
                    }
                    break;
                }
            }
            break;

            default: {
                this.sendHelp(sender);
            }
            break;
        }
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage(
                "&ebungee add <name> <version> <url>",
                "&ebungee remove <name> <version>",
                "&ebungee remove <name>"
        );
    }
}

package net.peepocloud.centralserver.web;
/*
 * Created by Mc_Ruben on 10.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.utility.Hashing;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class HttpHandler implements com.sun.net.httpserver.HttpHandler {

    private static final byte[] RATE_LIMIT = new SimpleJsonObject().append("success", false).append("reason", Collections.singletonList("You're rate limited")).toBytes();

    WebServer server;

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String banReason = CentralServer.getInstance().getBanManager().getBanReason(httpExchange.getRemoteAddress().getAddress().getHostAddress(), httpExchange.getRemoteAddress().getHostName());
        if (banReason != null) {
            send(
                    httpExchange,
                    403,
                    new SimpleJsonObject()
                            .append("success", false)
                            .append("reason", Arrays.asList("You were banned!", banReason))
                            .toBytes()
            );
            return;
        }
        handle1(Hashing.hashString(httpExchange.getRemoteAddress().getAddress().getHostAddress()), httpExchange);
    }

    public void handle1(String hashedIp, HttpExchange httpExchange) throws IOException {
        if (this.server.isRateLimited(hashedIp)) {
            send(
                    httpExchange,
                    403,
                    RATE_LIMIT
            );
            return;
        }

        this.server.createRateLimit(hashedIp);
        this.handle0(hashedIp, httpExchange);
    }

    public abstract void handle0(String hashedIp, HttpExchange httpExchange) throws IOException;

    protected void send(HttpExchange httpExchange, int responseCode, byte[] bytes) throws IOException {
        httpExchange.sendResponseHeaders(responseCode, bytes.length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(bytes);
        outputStream.close();
    }

    protected Map<String, String> getQueryParameters(HttpExchange httpExchange) {
        return new QueryDecoder(httpExchange.getRequestURI().getQuery()).query;
    }

    private class QueryDecoder {
        private Map<String, String> query = new HashMap<>();

        private QueryDecoder(String encode) {
            if (encode == null)
                return;

            if (!encode.contains("&")) {
                put(encode);
            } else {
                for (String split : encode.split("\\&")) {
                    put(split);
                }
            }
        }

        private void put(String input) {
            String[] output = input.split("\\=");
            if (output.length == 2)
                query.put(output[0], output[1]);
        }
    }

}

package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 05.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

import java.io.IOException;
import java.util.Arrays;

public class CommandAddon extends Command {
    public CommandAddon() {
        super("addon");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            sender.sendMessage("&7Installed addons:");
            CentralServer.getInstance().getAddons().forEach(addon -> {
                sender.sendMessage("&7 - &e" + addon.getName() + "&7:&e" + addon.getVersion() + " &7(&e" + Arrays.toString(addon.getAllVersions()) + "&7) by &e" + Arrays.toString(addon.getAuthors()));
            });
        } else if (args.length == 6 && args[0].equalsIgnoreCase("add")) {
            sender.sendMessage("&eTrying to install the addon...");
            CentralServer.getInstance().getExecutorService().execute(() -> {
                long start = System.nanoTime();
                try {
                    sender.sendMessage(CentralServer.getInstance().getAddonInstaller().addAddon(args[1], args[2], args[3].split(";"), args[4].split(";"), args[5]).formatMessage(System.nanoTime() - start));
                } catch (IOException e) {
                    sender.sendMessage("&cAn error occurred while attempting to install the addon");
                    e.printStackTrace();
                }
            });
        } else if (args.length == 3 && args[0].equalsIgnoreCase("update")) {
            //TODO complete this
        } else if (args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            //TODO complete this
        } else {
            sender.sendMessage(
                    "addon list",
                    "addon add <name> <version> <all_versions (split by \";\")> <authors (split by \";\")> <download_url>",
                    "addon update <name> <version> <all_versions (split by \";\")> <download_url>",
                    "addon remove <name>"
            );
        }
    }
}

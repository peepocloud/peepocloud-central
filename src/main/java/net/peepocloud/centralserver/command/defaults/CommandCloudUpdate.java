package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 08.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

import java.util.Arrays;

public class CommandCloudUpdate extends Command {
    public CommandCloudUpdate() {
        super("cloudupdate");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length != 1) {
            sender.sendMessage("&ecloudupdate <newVersion>");
            return;
        }

        String oldVersion = CentralServer.getInstance().getUpdatesManager().getNewestVersion();
        String newVersion = args[0];

        if (CentralServer.getInstance().getUpdatesManager().getVersions().contains(newVersion)) {
            sender.sendMessage("&cThere is already a version with that name");
            return;
        }

        CentralServer.getInstance().getExecutorService().execute(() -> {
            StringBuilder discordText = new StringBuilder();

            sender.sendMessage("&aShould there be a message on discord?");
            String a = CentralServer.getInstance().getLogger().readLine();
            boolean discordMessage = a.equals("true") || a.equals("yes") || a.equals("ja");
            if (discordMessage) {
                sender.sendMessage("&aType in the text for discord and type &efinish &aif you are done");
                CentralServer.getInstance().getLogger().readLinesUntil(s -> s.equalsIgnoreCase("finish"), s -> discordText.append(s).append('\n'));
            }

            CentralServer.getInstance().getAutoUpdaterManager().doUpdate(
                    newVersion,
                    discordText.length() > 0 ? discordText.substring(0, discordText.length() - 1) : "",
                    success -> {
                        if (success) {
                            sender.sendMessage("&aSuccessfully updated the system from &e" + oldVersion + " &ato &e" + newVersion);
                        } else {
                            sender.sendMessage("&cAn error occurred while attempting to update the system to &e" + newVersion);
                        }
                    },
                    discordMessage
            );
        });
    }
}

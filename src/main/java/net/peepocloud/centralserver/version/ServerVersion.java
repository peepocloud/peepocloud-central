package net.peepocloud.centralserver.version;
/*
 * Created by Mc_Ruben on 24.11.2018
 */

import lombok.*;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ServerVersion {
    private String version;
    private String url;
}

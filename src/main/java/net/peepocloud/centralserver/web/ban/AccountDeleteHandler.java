package net.peepocloud.centralserver.web.ban;
/*
 * Created by Mc_Ruben on 23.12.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.util.Collections;

public class AccountDeleteHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        if (!httpExchange.getRequestHeaders().containsKey("Peepo-Username") ||
                !httpExchange.getRequestHeaders().containsKey("Peepo-Token")) {
            send(
                    httpExchange,
                    403,
                    new SimpleJsonObject().append("success", false).append("reason", Collections.singletonList("Invalid auth")).toBytes()
            );
            return;
        }
        String username = httpExchange.getRequestHeaders().getFirst("Peepo-Username");
        String token = httpExchange.getRequestHeaders().getFirst("Peepo-Password");
        if (username == null || token == null)
            return;

        if (!CentralServer.getInstance().getLoginManager().checkAuthAPI(username, token)) {
            send(
                    httpExchange,
                    403,
                    new SimpleJsonObject().append("success", false).append("reason", Collections.singletonList("Invalid auth")).toBytes()
            );
            return;
        }

        CentralServer.getInstance().getLoginManager().delete(username);
        CentralServer.getInstance().getBanManager().banWithHashedIp(hashedIp, "Deleted");
        send(
                httpExchange,
                200,
                new SimpleJsonObject().append("success", true).toBytes()
        );
    }
}

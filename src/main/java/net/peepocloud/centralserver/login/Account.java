package net.peepocloud.centralserver.login;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.peepocloud.centralserver.utility.Hashing;
import net.peepocloud.centralserver.utility.SystemUtils;

import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Account implements Serializable {
    private String username;
    private String hashedPassword;
    private Collection<APICloudData> apiData;

    public boolean checkAuth(String username, String password) {
        return this.username.equals(username) && this.hashedPassword.equals(Hashing.hashString(password));
    }

    public boolean checkAuthAPI(String username, String uniqueId, String apiToken) {
        if (!this.username.equals(username))
            return false;
        for (APICloudData apiData : this.apiData) {
            if (apiData.getUniqueId().equals(uniqueId) && apiData.getApiToken().equals(apiToken))
                return true;
        }
        return false;
    }

    public boolean checkAuthAPI(String username, String apiToken) {
        if (!this.username.equals(username))
            return false;
        for (APICloudData apiData : this.apiData) {
            if (apiData.getApiToken().equals(apiToken))
                return true;
        }
        return false;
    }

    /**
     * Adds a new cloud if the maximum of 15 is not reached
     * @return a new {@link APICloudData} for the cloud or {@code null} if the maximum is reached
     */
    public APICloudData addCloud() {
        if (this.apiData.size() > 15)
            return null;
        APICloudData data = new APICloudData(
                SystemUtils.randomString(128),
                SystemUtils.randomString(512)
        );
        this.apiData.add(data);
        return data;
    }
}

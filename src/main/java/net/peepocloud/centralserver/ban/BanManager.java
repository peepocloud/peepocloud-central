package net.peepocloud.centralserver.ban;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import lombok.*;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.utility.Hashing;
import net.peepocloud.centralserver.utility.ObjectSerialization;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Getter
public class BanManager {

    private final Path path;
    private Map<String, Ban> bans = new HashMap<>();

    public BanManager(String file) {
        this.path = Paths.get(file);
        this.load();
    }

    public void load() {
        Object object = ObjectSerialization.deserialize(this.path);
        if (object != null) {
            this.bans = (Map<String, Ban>) object;
        }
    }

    public void save() {
        ObjectSerialization.serialize(this.bans, this.path);
    }

    public void ban(Ban ban) {
        ban.setIp(Hashing.hashString(ban.getIp()));
        this.bans.put(ban.getIp(), ban);
        this.save();

        CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByIp(ban.getIp());
    }

    public void banWithHashedIp(String hashedIp, String reason) {
        this.bans.put(hashedIp, new Ban(hashedIp, Ban.Type.IP, reason));
        this.save();
        CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByIp(hashedIp);
    }

    public void unban(String ip) {
        ip = Hashing.hashString(ip);
        if (this.bans.containsKey(ip)) {
            this.bans.remove(ip);
            this.save();
        }
    }

    public String getBanReason(String ip, String hostname) {
        ip = Hashing.hashString(ip);
        if (this.bans.containsKey(ip)) {
            Ban ban = this.bans.get(ip);
            return ban != null && ban.getType().equals(Ban.Type.IP) ? ban.getReason() : null;
        }
        hostname = Hashing.hashString(hostname);
        if (this.bans.containsKey(hostname)) {
            Ban ban = this.bans.get(hostname);
            return ban != null && ban.getType().equals(Ban.Type.HOSTNAME) ? ban.getReason() : null;
        }
        return null;
    }

    public boolean isBanned(String ip, Ban.Type type) {
        ip = Hashing.hashString(ip);
        return this.bans.containsKey(ip) && this.bans.get(ip).getType().equals(type);
    }

    public boolean isBanned(String ip, String hostname) {
        return isBanned(ip) || isBanned(hostname);
    }

    public boolean isBanned(String ip) {
        ip = Hashing.hashString(ip);
        return this.bans.containsKey(ip);
    }

}

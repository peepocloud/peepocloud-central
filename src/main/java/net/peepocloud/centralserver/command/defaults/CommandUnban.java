package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;
import net.peepocloud.centralserver.command.TabCompletable;

import java.util.Collection;

public class CommandUnban extends Command implements TabCompletable {
    public CommandUnban() {
        super("unban");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("&eunban <ip>");
            return;
        }

        String ip = args[0];
        if (!CentralServer.getInstance().getBanManager().isBanned(ip)) {
            sender.sendMessage("&cThis ip/hostname is already banned");
            return;
        }

        CentralServer.getInstance().getBanManager().unban(ip);

        sender.sendMessage("&aSuccessfully unbanned &e" + ip);
    }

    @Override
    public Collection<String> tabComplete(CommandSender sender, String commandLine, String[] args) {
        return CentralServer.getInstance().getBanManager().getBans().keySet();
    }
}

package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 22.12.2018
 */

import com.google.common.base.Joiner;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;

public class CommandVersions extends DiscordCommand {
    public CommandVersions() {
        super("versions");
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        message.getChannel().sendMessage("PeepoCloud has the following versions released: **" +
                Joiner.on("**, **").join(CentralServer.getInstance().getUpdatesManager().getVersions()) + "**"
        ).queue();
    }
}

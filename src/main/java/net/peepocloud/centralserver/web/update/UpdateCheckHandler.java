package net.peepocloud.centralserver.web.update;
/*
 * Created by Mc_Ruben on 10.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.util.Map;

public class UpdateCheckHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        Map<String, String> query = getQueryParameters(httpExchange);
        if (!query.containsKey("version")) {
            send(
                    httpExchange,
                    200,
                    new SimpleJsonObject().append("success", false).append("reason", "Missing version parameter").toBytes()
            );
            return;
        }

        String version = query.get("version");

        int versionsBehind = CentralServer.getInstance().getUpdatesManager().getVersionsBehind(version);

        send(
                httpExchange,
                200,
                new SimpleJsonObject()
                        .append("success", true)
                        .append("versionsBehind", versionsBehind)
                        .append("upToDate", versionsBehind == 0)
                        .append("newestVersion", CentralServer.getInstance().getUpdatesManager().getNewestVersion())
                        .toBytes()
        );
    }
}

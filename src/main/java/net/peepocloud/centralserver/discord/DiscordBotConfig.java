package net.peepocloud.centralserver.discord;
/*
 * Created by Mc_Ruben on 25.11.2018
 */

import com.google.common.reflect.TypeToken;
import lombok.Data;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import net.peepocloud.centralserver.discord.presence.DiscordConfigGame;
import net.peepocloud.centralserver.json.SimpleJsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Data
public class DiscordBotConfig {

    private final String path;

    private String token;
    private long guildId;
    private long updateChannelId;
    private long adminRoleId;
    private long moderatorRoleId;
    private List<DiscordConfigGame> games;

    private long supportRequestChannelIdDE;
    private long supportChannelIdDE;
    private long supportingRoleIdDE;
    private long supportRequestChannelIdEN;
    private long supportChannelIdEN;
    private long supportingRoleIdEN;

    public DiscordBotConfig(String path) {
        this.path = path;
        this.load();
    }

    void load() {
        SimpleJsonObject jsonObject = SimpleJsonObject.load(this.path);
        this.token = jsonObject.getString("token");
        this.guildId = jsonObject.getLong("guildId");
        this.updateChannelId = jsonObject.getLong("updateChannelId");
        this.adminRoleId = jsonObject.getLong("adminRoleId");
        this.moderatorRoleId = jsonObject.getLong("moderatorRoleId");
        this.games = ((List<DiscordConfigGame>) jsonObject.getObject("games", new TypeToken<List<DiscordConfigGame>>() {
        }.getType()));
        if (this.games == null)
            this.games = new ArrayList<>();
        this.supportRequestChannelIdDE = jsonObject.getLong("supportRequestChannelIdDE");
        this.supportChannelIdDE = jsonObject.getLong("supportChannelIdDE");
        this.supportingRoleIdDE = jsonObject.getLong("supportingRoleIdDE");

        this.supportRequestChannelIdEN = jsonObject.getLong("supportRequestChannelIdEN");
        this.supportChannelIdEN = jsonObject.getLong("supportChannelIdEN");
        this.supportingRoleIdEN = jsonObject.getLong("supportingRoleIdEN");

        if (this.token == null) {
            jsonObject.append("token", "token")
                    .append("guildId", 1L)
                    .append("updateChannelId", 1L)
                    .append("adminRoleId", 1L)
                    .append("moderatorRoleId", 1L)
                    .append("games", new ArrayList<>())
                    .append("supportRequestChannelIdDE", 1L)
                    .append("supportChannelIdDE", 1L)
                    .append("supportingRoleIdDE", 1L)
                    .append("supportRequestChannelIdEN", 1L)
                    .append("supportChannelIdEN", 1L)
                    .append("supportingRoleIdEN", 1L)
                    .saveAsFile(this.path);
        }
    }

    public Game getRandomGame(JDA jda) {
        if (!this.games.isEmpty()) {
            return this.games.get(ThreadLocalRandom.current().nextInt(this.games.size())).toGame(jda);
        }
        return null;
    }

}

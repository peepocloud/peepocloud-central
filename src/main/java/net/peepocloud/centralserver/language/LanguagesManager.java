package net.peepocloud.centralserver.language;
/*
 * Created by Mc_Ruben on 07.11.2018
 */

import lombok.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Data
@AllArgsConstructor
public class LanguagesManager {

    private Map<String, Language> languages = new HashMap<>();

    public LanguagesManager() {
        this.reload();
    }

    public void reload() {
        this.languages.clear();

        Path path = Paths.get("language");
        if (!Files.exists(path))
            return;

        try {
            Files.walkFileTree(
                    path,
                    EnumSet.noneOf(FileVisitOption.class),
                    Integer.MAX_VALUE,
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (!Files.isDirectory(file) && file.toString().endsWith(".properties")) {
                                Properties properties = new Properties();
                                InputStream inputStream = Files.newInputStream(file);
                                properties.load(inputStream);
                                inputStream.close();
                                Language language = Language.load(properties);
                                languages.put(language.getName(), language);

                                System.out.println("&aSuccessfully loaded language &e" + language.getName() + " (" + language.getShortName() + ")&a with &e" + language.getMessages().size() + " messages");
                            }
                            return FileVisitResult.CONTINUE;
                        }
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Language getLanguageByName(String name) {
        return this.languages.get(name);
    }

    public Language getLanguageByShortName(String shortName) {
        for (Language language : this.languages.values())
            if (language.getShortName().equalsIgnoreCase(shortName))
                return language;
        return null;
    }

}

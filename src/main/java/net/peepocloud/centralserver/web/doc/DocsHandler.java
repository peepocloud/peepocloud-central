package net.peepocloud.centralserver.web.doc;
/*
 * Created by Mc_Ruben on 01.12.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DocsHandler extends HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        URI uri = httpExchange.getRequestURI();
        String path = uri.getPath();
        if (path == null) {
            send(httpExchange, 400, new byte[0]);
            return;
        }
        if (path.contains("../")) {
            send(httpExchange, 404, new byte[0]);
            return;
        }
        path = path.substring(1);
        boolean a = false;
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
            a = true;
        }
        Path file = Paths.get(path);
        if (Files.isDirectory(file)) {
            if (a) {
                path += "/index.html";
                file = Paths.get(path);
                if (!Files.exists(file) || Files.isDirectory(file)) {
                    send(httpExchange, 404, new byte[0]);
                    return;
                }
            } else {
                String host = httpExchange.getRequestHeaders().getFirst("Host");
                if (host != null) {
                    send(
                            httpExchange,
                            200,
                            ("<head><meta http-equiv=\"refresh\" content=\"0; URL=http://" + host + "/" + path + "/\"></head><body></body>").getBytes(StandardCharsets.UTF_8)
                    );
                    return;
                }
            }
        }

        httpExchange.getResponseHeaders().set("Content-Type", MimeTypes.getType(path));
        send(httpExchange, 200, Files.readAllBytes(file));

    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) { }

}

package net.peepocloud.centralserver.web.login;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.util.Collections;

public class LoginHandler extends HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Headers headers = httpExchange.getRequestHeaders();
        if (!headers.containsKey("Peepo-UserName") || !headers.containsKey("Peepo-Password")) {
            send(
                    httpExchange,
                    400,
                    new SimpleJsonObject().append("success", false).append("reason", Collections.singletonList("Missing \"Peepo-UserName\" or \"Peepo-Password\" header")).toBytes()
            );
            return;
        }

        String username = headers.getFirst("Peepo-UserName");
        String password = headers.getFirst("Peepo-Password");
        boolean success = username != null && password != null && CentralServer.getInstance().getLoginManager().checkAuth(username, password);
        send(
                httpExchange,
                200,
                new SimpleJsonObject()
                        .append("success", true)
                        .append("response", success)
                        .toBytes()
        );
    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException { }
}

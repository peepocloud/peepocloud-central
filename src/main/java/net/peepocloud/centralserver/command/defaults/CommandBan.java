package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.ban.Ban;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

public class CommandBan extends Command {
    public CommandBan() {
        super("ban");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("&eban <ip>");
            return;
        }

        String ip = args[0];
        Ban.Type type = Ban.Type.IP;
        if (ip.split("\\.").length != 4) {
            sender.sendMessage("&cIP did not contain &e3 \".\"&c, checking for hostname");
            type = Ban.Type.HOSTNAME;
        }
        if (CentralServer.getInstance().getBanManager().isBanned(ip, type)) {
            sender.sendMessage("&cThis ip/hostname is already banned");
            return;
        }

        sender.sendMessage("&aPlease type in the reason for the ban or &ecancel &ato cancel the operation:");
        String reason = CentralServer.getInstance().getLogger().readLine();
        if (reason.equalsIgnoreCase("cancel")) {
            sender.sendMessage("&cYou cancelled the operation");
            return;
        }

        CentralServer.getInstance().getBanManager().ban(new Ban(ip, type, reason));

        sender.sendMessage("&aSuccessfully banned &e" + ip + " &afor &e" + type + " &awith the reason: &e" + reason);
    }
}

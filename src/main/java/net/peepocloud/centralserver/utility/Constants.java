package net.peepocloud.centralserver.utility;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

public class Constants {
    private Constants() { }

    public static final int MILLIS_PER_TICK = 50;
    public static final int MILLIS_SUPPORT_RATELIMIT = 25000;
}

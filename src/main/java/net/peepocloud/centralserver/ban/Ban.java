package net.peepocloud.centralserver.ban;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Ban implements Serializable {

    private String ip;
    private Type type;
    private String reason;

    public enum Type implements Serializable {
        IP, HOSTNAME
    }

}

package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;

import java.util.concurrent.TimeUnit;

public class CommandSupportEnd extends DiscordCommand {
    public CommandSupportEnd() {
        super("supportend", "se");
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        if (CentralServer.getInstance().getDiscordBot().getSupportManager().handleSupportEndRequest(member)) {
            message.getChannel().sendMessage("Your support has been closed").queue(message1 -> {
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
                message.delete().queue();
            });
        } else {
            message.getChannel().sendMessage("You don't have any support conversations opened at this moment").queue(message1 -> {
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
                message.delete().queue();
            });
        }
    }
}

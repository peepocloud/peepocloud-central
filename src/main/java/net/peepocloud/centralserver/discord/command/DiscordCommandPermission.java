package net.peepocloud.centralserver.discord.command;
/*
 * Created by Mc_Ruben on 01.12.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.peepocloud.centralserver.CentralServer;

import java.util.function.Function;

public enum DiscordCommandPermission {

    DEVEOPER(member -> {
        long id = member.getUser().getIdLong();
        return id == 282946450579652608L /*Panamo*/ || id == 357959970664480770L /*Rupen*/;
    }),
    ADMIN(member -> {
        for (Role role : member.getRoles()) {
            if (role.getIdLong() == CentralServer.getInstance().getDiscordBot().getConfig().getAdminRoleId()) {
                return true;
            }
        }
        return DEVEOPER.canExecute(member);
    }),
    MODERATOR(member -> {
        for (Role role : member.getRoles()) {
            if (role.getIdLong() == CentralServer.getInstance().getDiscordBot().getConfig().getModeratorRoleId()) {
                return true;
            }
        }
        return ADMIN.canExecute(member);
    }),
    USER(member -> true);

    private Function<Member, Boolean> canExecute;

    DiscordCommandPermission(Function<Member, Boolean> canExecute) {
        this.canExecute = canExecute;
    }

    public boolean canExecute(Member member) {
        return this.canExecute.apply(member);
    }

}

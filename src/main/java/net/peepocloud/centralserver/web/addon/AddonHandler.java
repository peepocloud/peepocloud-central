package net.peepocloud.centralserver.web.addon;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class AddonHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        byte[] bytes = CentralServer.getInstance().getGson().toJson(CentralServer.getInstance().getAddons()).getBytes(StandardCharsets.UTF_8);
        send(httpExchange, 200, bytes);
    }
}

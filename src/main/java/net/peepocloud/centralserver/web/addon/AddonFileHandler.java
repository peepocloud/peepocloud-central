package net.peepocloud.centralserver.web.addon;
/*
 * Created by Mc_Ruben on 06.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.addons.Addon;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

public class AddonFileHandler extends HttpHandler {
    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) throws IOException {
        Map<String, String> query = getQueryParameters(httpExchange);
        String addonName = query.get("addonName");
        String addonVersion = query.get("addonVersion");
        if (addonName == null || addonVersion == null) {
            send(httpExchange, 200, new SimpleJsonObject().append("success", false).append("message", "&cThis addon was not found on the server").toPrettyBytes());
            return;
        }
        Addon addon = CentralServer.getInstance().getAddon(addonName, addonVersion);
        if (addon == null) {
            send(httpExchange, 200, new SimpleJsonObject().append("success", false).append("message", "&cThis addon was not found on the server").toPrettyBytes());
            return;
        }
        send(httpExchange, 200, Files.readAllBytes(addon.getPath()));
    }
}

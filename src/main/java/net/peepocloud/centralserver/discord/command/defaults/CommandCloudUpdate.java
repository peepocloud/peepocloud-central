package net.peepocloud.centralserver.discord.command.defaults;
/*
 * Created by Mc_Ruben on 08.11.2018
 */

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.discord.command.DiscordCommand;
import net.peepocloud.centralserver.discord.command.DiscordCommandPermission;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class CommandCloudUpdate extends DiscordCommand {
    public CommandCloudUpdate() {
        super("cloudupdate", DiscordCommandPermission.DEVEOPER);
    }

    @Override
    public void execute(Member member, Message message, String[] args) {
        if (args.length != 1) {
            message.delete().queue();
            message.getChannel().sendMessage("!cloudupdate <newVersion>").queue(message1 -> {
                message1.delete().queueAfter(3, TimeUnit.SECONDS);
            });
            return;
        }

        String oldVersion = CentralServer.getInstance().getUpdatesManager().getNewestVersion();
        String newVersion = args[0];

        if (CentralServer.getInstance().getUpdatesManager().getVersions().contains(newVersion)) {
            message.delete().queue();
            message.getChannel().sendMessage("There is already a version with that name").queue(message1 -> message1.delete().queueAfter(3, TimeUnit.SECONDS));
            return;
        }

        CentralServer.getInstance().getDiscordBot().doAutoUpdate(message, member, oldVersion, newVersion);
    }

}

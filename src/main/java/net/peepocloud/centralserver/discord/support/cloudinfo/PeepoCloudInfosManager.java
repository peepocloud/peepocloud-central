package net.peepocloud.centralserver.discord.support.cloudinfo;
/*
 * Created by Mc_Ruben on 04.12.2018
 */

import lombok.Getter;
import net.peepocloud.centralserver.CentralServer;

import java.util.HashMap;
import java.util.Map;

@Getter
public class PeepoCloudInfosManager {

    private Map<String, PeepoCloudInfo> infos = new HashMap<>();

    public PeepoCloudInfo getInfo(String uniqueId) {
        return this.infos.get(uniqueId);
    }

    public boolean updateInfo(String hashedIp, PeepoCloudInfo info) {
        if (info == null || info.getUniqueId() == null || info.getCloudVersion() == null || info.getOs() == null)
            return false;
        int i = 0;
        if (!this.infos.containsKey(info.getUniqueId())) {
            for (PeepoCloudInfo value : this.infos.values()) {
                if (value.getHashedIpAddress().equals(hashedIp) || (System.currentTimeMillis() - value.getLastUpdate()) < 5000) {
                    if (i++ > 15)
                        return false;
                }
            }
        }

        info.setLastUpdate(System.currentTimeMillis());
        info.setHashedIpAddress(hashedIp);
        this.infos.put(info.getUniqueId(), info);
        return true;
    }

    public boolean removeInfoByIp(String hashedIp) {
        PeepoCloudInfo remove = null;
        for (PeepoCloudInfo value : this.infos.values()) {
            if (value.getHashedIpAddress().equals(hashedIp)) {
                remove = value;
                break;
            }
        }
        if (remove != null)
            return this.infos.remove(remove.getUniqueId(), remove);
        return false;
    }

    public boolean removeInfoByUniqueId(String uniqueId) {
        return this.infos.remove(uniqueId) != null;
    }
}

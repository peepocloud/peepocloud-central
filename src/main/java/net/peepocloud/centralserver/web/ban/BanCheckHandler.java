package net.peepocloud.centralserver.web.ban;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import com.sun.net.httpserver.HttpExchange;
import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.json.SimpleJsonObject;
import net.peepocloud.centralserver.web.HttpHandler;

import java.io.IOException;

public class BanCheckHandler extends HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String banReason = CentralServer.getInstance().getBanManager().getBanReason(httpExchange.getRemoteAddress().getAddress().getHostAddress(), httpExchange.getRemoteAddress().getHostName());
        send(
                httpExchange,
                200,
                new SimpleJsonObject()
                        .append("success", true)
                        .append("response",
                                new SimpleJsonObject()
                                        .append("banned", banReason != null)
                                        .append("reason", banReason)
                                        .asJsonObject()
                        )
                        .toBytes()
        );
    }

    @Override
    public void handle0(String hashedIp, HttpExchange httpExchange) { }
}

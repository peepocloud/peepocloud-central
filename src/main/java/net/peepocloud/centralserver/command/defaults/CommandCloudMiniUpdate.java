package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 08.11.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;

import java.util.Arrays;

public class CommandCloudMiniUpdate extends Command {
    public CommandCloudMiniUpdate() {
        super("cloudminiupdate");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        String version = CentralServer.getInstance().getUpdatesManager().getNewestVersion();

        CentralServer.getInstance().getExecutorService().execute(() -> {
            StringBuilder discordText = new StringBuilder();

            sender.sendMessage("&aShould there be a message on discord?");
            String a = CentralServer.getInstance().getLogger().readLine();
            boolean discordMessage = a.equals("true") || a.equals("yes") || a.equals("ja");
            if (discordMessage) {
                sender.sendMessage("&aType in the text for discord and type &efinish &aif you are done");
                CentralServer.getInstance().getLogger().readLinesUntil(s -> s.equalsIgnoreCase("finish"), s -> discordText.append(s).append('\n'));
            }

            CentralServer.getInstance().getAutoUpdaterManager().doUpdate(
                    version,
                    discordText.length() > 0 ? discordText.substring(0, discordText.length() - 1) : "",
                    success -> {
                        if (success) {
                            sender.sendMessage("&aSuccessfully updated the system from &e" + version + " &ato &e" + version);
                        } else {
                            sender.sendMessage("&cAn error occurred while attempting to update the system to &e" + version);
                        }
                    },
                    discordMessage
            );
        });
    }
}

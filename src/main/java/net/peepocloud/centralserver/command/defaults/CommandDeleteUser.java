package net.peepocloud.centralserver.command.defaults;
/*
 * Created by Mc_Ruben on 23.12.2018
 */

import net.peepocloud.centralserver.CentralServer;
import net.peepocloud.centralserver.ban.Ban;
import net.peepocloud.centralserver.command.Command;
import net.peepocloud.centralserver.command.CommandSender;
import net.peepocloud.centralserver.discord.support.cloudinfo.PeepoCloudInfo;
import net.peepocloud.centralserver.utility.Hashing;

public class CommandDeleteUser extends Command {

    public CommandDeleteUser() {
        super("deleteuser", null, "delete");
    }

    @Override
    public void execute(CommandSender sender, String commandLine, String[] args) {
        if (args.length != 2) {
            sender.sendMessage("&edelete IP|UNIQUE <ip/uniqueId>");
            return;
        }

        if (args[0].equalsIgnoreCase("ip")) {
            String ip = args[1];
            if (CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByIp(Hashing.hashString(ip))) {
                sender.sendMessage("&aSuccessfully removed the cloud info for the ip &e" + ip + " &aout of the memory");
                CentralServer.getInstance().getBanManager().ban(new Ban(ip, Ban.Type.IP, "Deleted"));
            } else {
                sender.sendMessage("&cNo cloud info for the ip &e" + ip + " &cwas found in the memory");
            }
        } else if (args[0].equalsIgnoreCase("unique")) {
            String uniqueId = args[1];
            PeepoCloudInfo info = CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().getInfo(uniqueId);
            if (CentralServer.getInstance().getDiscordBot().getSupportManager().getCloudInfosManager().removeInfoByUniqueId(uniqueId)) {
                CentralServer.getInstance().getBanManager().banWithHashedIp(info.getHashedIpAddress(), "Deleted");
                CentralServer.getInstance().getLoginManager().delete(info.getUniqueId());
                sender.sendMessage("&aSuccessfully removed the cloud info for the uniqueId &e" + uniqueId + " &aout of the memory");
            } else {
                sender.sendMessage("&cNo cloud info for the uniqueId &e" + uniqueId + " &cwas found in the memory");
            }
        } else {
            sender.sendMessage("&edelete IP|UNIQUE <ip/uniqueId>");
        }
    }
}

package net.peepocloud.centralserver.login;
/*
 * Created by Mc_Ruben on 05.12.2018
 */

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class APICloudData implements Serializable {
    private String uniqueId;
    private String apiToken;
}

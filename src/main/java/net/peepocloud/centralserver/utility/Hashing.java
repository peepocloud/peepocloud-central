package net.peepocloud.centralserver.utility;
/*
 * Created by Mc_Ruben on 26.11.2018
 */

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Hashing {

    private Hashing() { }

    /**
     * Hashes the specified {@link String}
     * @param input the {@link String} to hash
     * @return the hashed {@link String} or if an error occurs the input {@link String}
     */
    public static String hashString(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(input.getBytes(StandardCharsets.UTF_8));
            return new String(Base64.getMimeEncoder().encode(digest.digest()), StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return input;
    }

}

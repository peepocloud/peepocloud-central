package net.peepocloud.centralserver.autoupdater;
/*
 * Created by Mc_Ruben on 15.12.2018
 */

import lombok.*;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class AddonModule {
    private String name;
    private String version;
    private String[] versions;
    private String[] authors;
    private String path;
}

package net.peepocloud.centralserver.discord.command;
/*
 * Created by Mc_Ruben on 01.12.2018
 */

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DiscordCommandManager {

    private Map<String, DiscordCommand> commands = new HashMap<>();

    public DiscordCommandManager(JDA jda) {
        jda.addEventListener((EventListener) event -> {
            if (event instanceof GuildMessageReceivedEvent) {
                dispatchCommand(((GuildMessageReceivedEvent) event).getMessage());
            }
        });
    }

    public boolean dispatchCommand(Message message) {
        if (message.getAuthor().isBot() || message.getContentRaw().length() == 0 || message.getContentRaw().charAt(0) != '!')
            return false;
        String[] a = message.getContentRaw().substring(1).split(" ");
        if (a.length <= 0)
            return false;
        String commandName = a[0].toLowerCase();
        DiscordCommand command = this.commands.get(commandName);
        if (command == null)
            return false;
        if (command.getPermission() != null && !command.getPermission().canExecute(message.getMember())) {
            message.getChannel().sendMessage("You are not allowed, to use this command").queue(message1 -> {
                message.delete().queue();
                message1.delete().queueAfter(5, TimeUnit.SECONDS);
            });
            return false;
        }
        command.execute(message.getMember(), message, Arrays.copyOfRange(a, 1, a.length));
        return true;
    }

    public DiscordCommand getCommand(String name) {
        return this.commands.get(name.toLowerCase());
    }

    public DiscordCommandManager registerCommands(DiscordCommand... commands) {
        for (DiscordCommand command : commands) {
            this.commands.put(command.getName().toLowerCase(), command);
            if (command.getAliases() != null && command.getAliases().length != 0) {
                for (String alias : command.getAliases()) {
                    this.commands.put(alias.toLowerCase(), command);
                }
            }
        }
        return this;
    }

}
